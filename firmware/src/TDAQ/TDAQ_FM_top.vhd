library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.types_pkg.all;
Library UNISIM;
use UNISIM.vcomponents.all;
Library xpm;
use xpm.vcomponents.all;

entity TDAQ_top is
	generic(
		n_staves : natural := 15;
		n_therm  : natural := 16
	);
	port(
		sysclk   : in std_logic;
		--trigger iface
		trig_evnt : in std_logic;
		trig_in   : in std_logic_vector(4 downto 0);
		busy_out  : out std_logic;
		-- DCPU GPIO
		reset  : in std_logic;
		PPS    : in std_logic;
		gpio_1 : in std_logic;
		gpio_2 : out std_logic;
		-- ALPIDE pins
		ALP_clk    : out std_logic_vector(n_staves-1 downto 0);
		ALP_ctrl_R : in std_logic_vector(n_staves-1 downto 0);
		ALP_ctrl_D : out std_logic_vector(n_staves-1 downto 0);
		ALP_ctrl_T : out std_logic_vector(n_staves-1 downto 0);
		-- spacewire interface
		spw_di : in  std_logic;
		spw_si : in  std_logic;
		spw_do : out std_logic;
		spw_so : out std_logic;
		-- QSPI
		qspi_dq  : inout std_logic_vector(3 downto 0);
		qspi_cs  : inout std_logic;
		qspi_sck : inout std_logic;
		-- Onewire temp sensor
		onewire   : inout std_logic_vector(n_therm-1 downto 0);
		-- TSP switches
		TSP_EN_A     : out std_logic_vector(14 downto 0);
		TSP_EN_D     : out std_logic_vector(14 downto 0);
		TSP_EN_BIAS  : out std_logic_vector(14 downto 0);
		TSP_pwr_good : in std_logic_vector(14 downto 0);
		-- SRAM
		SRAM_BLE : out std_logic;
		SRAM_BHE : out std_logic;
		SRAM_OE  : out std_logic;
		SRAM_WE  : out std_logic;
		SRAM_CE  : out std_logic;
		SRAM_A   : out std_logic_vector(20 downto 0);
		SRAM_IO  : inout std_logic_vector(15 downto 0);
		-- SEM mon shm
		SEM_mon_tx : out std_logic;
		SEM_mon_rx : in std_logic
	);
end TDAQ_top;


architecture Behavioral of TDAQ_top is

	signal clk_40, clk_80 : std_logic;
	signal rst_80, rst_daq, rst_time : std_logic;
	signal trig_turret, trig_mask : std_logic_vector(4 downto 0);
	signal trig_mask_ev : std_logic;
	signal busy : std_logic_vector(n_staves-1 downto 0);
	signal global_busy : std_logic;
	signal rst_reg : std_logic_vector(31 downto 0);

	-- ALPIDE FSM interface
	signal alp_cmd : std_logic_vector(47 downto 0);
	signal alp_cmd_res : slv16_arr(0 to n_staves-1);
	signal alp_cmd_res_DV, alp_cmd_ack, alp_cmd_err: std_logic_vector(n_staves-1 downto 0);
	signal alp_cmd_valid: std_logic_vector(n_staves-1 downto 0);
	-- bus multiplexer
	signal data_out : slv8_arr(0 to n_staves-1);
	signal data_out_valid, data_out_last, data_out_ready : std_logic_vector(n_staves-1 downto 0);

	-- external comunication i/o buffers
	signal spw_bram_we : std_logic_vector(3 downto 0);
	signal spw_bram_addr, spw_bram_din, spw_bram_dout : std_logic_vector(31 downto 0);
	signal pkgr_valid, pkgr_ready, pkgr_last : std_logic;
	signal pkgr_data : std_logic_vector(31 downto 0);

	signal daq_run, cmd_intr_mcu : std_logic;

	-- ctrl flags
	signal out_mux_en, dec_skip_data : std_logic;
	signal stave_mask, clk_gate_dis : std_logic_vector(n_staves-1 downto 0);
	signal trig_count : std_logic_vector(31 downto 0);
	signal cpu_alarms : std_logic_vector(15 downto 0);
	signal busy_viol_cnt : std_logic_vector(15 downto 0);
	signal timestamp : std_logic_vector(31 downto 0);
	signal trigger_lut : std_logic_vector(31 downto 0);

	-- SEM monitor
	signal SEM_flags, SEM_flags_sync : std_logic_vector(3 downto 0);

	-- MCU pixdata FIFO Buffer
	signal axis_pix_tdata : std_logic_vector(31 downto 0);
	signal axis_pix_tready, axis_pix_tvalid, axis_pix_tlast : std_logic;
	-- MCU AXI port
	signal axi_rst_out : std_logic_vector(0 downto 0);
	signal AXI_ctrl_WDATA, AXI_ctrl_RDATA : std_logic_vector(31 downto 0);
	signal AXI_ctrl_WSTRB : std_logic_vector(3 downto 0);
	signal AXI_ctrl_AWADDR, AXI_ctrl_ARADDR : std_logic_vector(31 downto 0);
	signal AXI_ctrl_BRESP, AXI_ctrl_RRESP : std_logic_vector(1 downto 0);
	signal AXI_ctrl_AWVALID, AXI_ctrl_AWREADY, AXI_ctrl_WVALID, AXI_ctrl_WREADY, AXI_ctrl_BVALID,
		AXI_ctrl_BREADY, AXI_ctrl_ARVALID, AXI_ctrl_ARREADY, AXI_ctrl_RVALID, AXI_ctrl_RREADY
		: std_logic;
	-- MCU QSPI ports
	signal qspi_dq_i, qspi_dq_o, qspi_dq_t : std_logic_vector(3 downto 0);
	signal qspi_cs_i, qspi_cs_o, qspi_cs_t : std_logic;
	signal qspi_sck_i, qspi_sck_o, qspi_sck_t : std_logic;
	-- MCU OneWire
	signal owr_i, owr_o, owr_t : std_logic_vector(n_therm-1 downto 0);
	-- SRAM
	signal EMC_INTF_dq_i, EMC_INTF_dq_o, EMC_INTF_dq_t : std_logic_vector(15 downto 0);
	signal EMC_INTF_addr : std_logic_vector(31 downto 0);


	component BD_CPU is
		port(
			axi_rst_out : out std_logic_vector(0 to 0);
			clk_40   : in std_logic;
			clk_80   : in std_logic;
			reset_rtl : in std_logic;
			cmd_intr_mcu : in std_logic;
			axi_ctrl_awaddr  : out std_logic_vector(31 downto 0);
			axi_ctrl_awprot  : out std_logic_vector(2 downto 0);
			axi_ctrl_awvalid : out std_logic;
			axi_ctrl_awready : in std_logic;
			axi_ctrl_wdata   : out std_logic_vector(31 downto 0);
			axi_ctrl_wstrb   : out std_logic_vector(3 downto 0);
			axi_ctrl_wvalid  : out std_logic;
			axi_ctrl_wready  : in std_logic;
			axi_ctrl_bresp   : in std_logic_vector(1 downto 0);
			axi_ctrl_bvalid  : in std_logic;
			axi_ctrl_bready  : out std_logic;
			axi_ctrl_araddr  : out std_logic_vector(31 downto 0);
			axi_ctrl_arprot  : out std_logic_vector(2 downto 0);
			axi_ctrl_arvalid : out std_logic;
			axi_ctrl_arready : in std_logic;
			axi_ctrl_rdata   : in std_logic_vector(31 downto 0);
			axi_ctrl_rresp   : in std_logic_vector(1 downto 0);
			axi_ctrl_rvalid  : in std_logic;
			axi_ctrl_rready  : out std_logic;
			EMC_INTF_addr    : out std_logic_vector(31 downto 0);
			EMC_INTF_adv_ldn : out std_logic;
			EMC_INTF_ben     : out std_logic_vector(1 downto 0);
			EMC_INTF_ce      : out std_logic_vector(0 downto 0);
			EMC_INTF_ce_n    : out std_logic_vector(0 downto 0);
			EMC_INTF_clken   : out std_logic;
			EMC_INTF_cre     : out std_logic;
			EMC_INTF_dq_i    : in std_logic_vector(15 downto 0);
			EMC_INTF_dq_o    : out std_logic_vector(15 downto 0);
			EMC_INTF_dq_t    : out std_logic_vector(15 downto 0);
			EMC_INTF_lbon    : out std_logic;
			EMC_INTF_oen     : out std_logic_vector(0 downto 0);
			EMC_INTF_qwen    : out std_logic_vector(1 downto 0);
			EMC_INTF_rnw     : out std_logic;
			EMC_INTF_rpn     : out std_logic;
			EMC_INTF_wait    : in std_logic_vector(0 downto 0);
			EMC_INTF_wen     : out std_logic;
			spw_bram_clk  : in std_logic;
			spw_bram_rst  : in std_logic;
			spw_bram_en   : in std_logic;
			spw_bram_we   : in std_logic_vector(3 downto 0);
			spw_bram_addr : in std_logic_vector(31 downto 0);
			spw_bram_din  : in std_logic_vector(31 downto 0);
			spw_bram_dout : out std_logic_vector(31 downto 0);
			qspi_flash_io0_i : in std_logic;
			qspi_flash_io0_o : out std_logic;
			qspi_flash_io0_t : out std_logic;
			qspi_flash_io1_i : in std_logic;
			qspi_flash_io1_o : out std_logic;
			qspi_flash_io1_t : out std_logic;
			qspi_flash_io2_i : in std_logic;
			qspi_flash_io2_o : out std_logic;
			qspi_flash_io2_t : out std_logic;
			qspi_flash_io3_i : in std_logic;
			qspi_flash_io3_o : out std_logic;
			qspi_flash_io3_t : out std_logic;
			qspi_flash_ss_i  : in std_logic;
			qspi_flash_ss_o  : out std_logic;
			qspi_flash_ss_t  : out std_logic;
			qspi_flash_sck_i : in std_logic;
			qspi_flash_sck_o : out std_logic;
			qspi_flash_sck_t : out std_logic;
			owr_i : in std_logic_vector(n_therm-1 downto 0);
			owr_o : out std_logic_vector(n_therm-1 downto 0);
			owr_t : out std_logic_vector(n_therm-1 downto 0);
			axi_str_pixdata_tvalid : in std_logic;
			axi_str_pixdata_tready : out std_logic;
			axi_str_pixdata_tdata  : in std_logic_vector(31 downto 0);
			axi_str_pixdata_tlast  : in std_logic
		);
	end component BD_CPU;


begin

	------------------------------------------------
	----------------- CLK and PLLs -----------------
	------------------------------------------------

	clk_mng_inst: entity work.clock_manager
		port map(
			clk_osc => sysclk,
			clk_40  => clk_40,
			clk_80  => clk_80
		);

	sem_inst: entity work.SEM
		port map(
			clk => clk_40,
			SEM_flags => SEM_flags,
			monitor_tx => SEM_mon_tx,
			monitor_rx => SEM_mon_rx
		);

	------------------------------------------------
	------------ control and data mux --------------
	------------------------------------------------

	cmd_dec: entity work.cmd_decoder
		port map(
			clk_alp  => clk_40,
			clk_pkgr => clk_80,
			ext_rst => reset,
			rst_reg => rst_reg,
			rst_80  => rst_80,
			rst_daq => rst_daq,
			rst_time => rst_time,
			-- trigger
			daq_run   => daq_run,
			busy      => global_busy,
			trig_evnt => trig_evnt,
			trig_in   => trig_in,
			trig_out  => trig_turret,
			trig_mask => trig_mask,
			trig_mask_ev  => trig_mask_ev,
			trig_cnt_out  => trig_count,
			busy_viol_cnt => busy_viol_cnt,
			trigger_lut   => trigger_lut
		);


	timestamp_inst: entity work.pps_timestamp
		generic map(
			clk_freq      => 80,
			pps_reset_len => 6
		)
		port map(
			clk => clk_80,
			rst => rst_80 or rst_time,
			PPS => PPS,
			timestamp => timestamp
		);

	bus_mux: entity work.output_mux
		generic map(n_staves => n_staves)
		port map(
			clk => clk_80,
			rst => rst_daq,
			trig_mask_ev => trig_mask_ev,
			trig_mask  => trig_mask,
			trig_count => trig_count,
			timestamp  => timestamp,
			stave_mask => stave_mask,
			out_mux_en => out_mux_en,
			dec_skip_data => dec_skip_data,
			-- stave buffers
			data_in       => data_out,
			data_in_valid => data_out_valid,
			data_in_last  => data_out_last,
			din_rd        => data_out_ready,
			-- output data
			data_out  => pkgr_data,
			dout_DV   => pkgr_valid,
			dout_last => pkgr_last,
			dout_ack  => pkgr_ready,
			-- pix data to MCU
			axis_pix_tdata  => axis_pix_tdata,
			axis_pix_tlast  => axis_pix_tlast,
			axis_pix_tready => axis_pix_tready,
			axis_pix_tvalid => axis_pix_tvalid
		);

	------------------------------------------------
	------------ ALPIDE staves control -------------
	------------------------------------------------
	gen_staves: for N in 0 to n_staves-1 generate
		CTRL_mng: entity work.CTRL_controller
			port map(
				rst   => rst_daq,
				clk   => clk_40,
				rdclk => clk_80,
				-- Command interface
				trig    => trig_turret(N/3),
				busy    => busy(N),
				alp_cmd => alp_cmd,
				cmd_DV  => alp_cmd_valid(N),
				cmd_res => alp_cmd_res(N),
				cmd_ack => alp_cmd_ack(N),
				res_DV  => alp_cmd_res_DV(N),
				cmd_err => alp_cmd_err(N),
				-- Data output
				data_out       => data_out(N),
				data_out_last  => data_out_last(N),
				data_out_valid => data_out_valid(N),
				data_out_ready => data_out_ready(N),
				-- CTLR link
				ALP_clk    => ALP_clk(N),
				ALP_ctrl_R => ALP_ctrl_R(N),
				ALP_ctrl_D => ALP_ctrl_D(N),
				ALP_ctrl_T => ALP_ctrl_T(N),
				-- conf
				clk_gate_dis => clk_gate_dis(N)
			);
		end generate;

		global_busy <= (or busy);
		busy_out    <= not global_busy;

	------------------------------------------------
	------------- MCU and system i/o ---------------
	------------------------------------------------

	spw_comm_axis: entity work.spw_axis_wrapper
		generic map( sysfreq => 80.0e6)
		port map(
			clk => clk_80,
			rst => rst_80,
			rst_daq => rst_daq,
			-- spw pins
			spw_di => spw_di,
			spw_si => spw_si,
			spw_do => spw_do,
			spw_so => spw_so,
			-- gpio spw
			event_ready => gpio_2,
			event_clear => gpio_1,
			-- data AXIS interface
			pkgr_data  => pkgr_data,
			pkgr_valid => pkgr_valid,
			pkgr_ready => pkgr_ready,
			pkgr_last  => pkgr_last,
			-- mcu dpram iface
			mcu_bram_we   => spw_bram_we,
			mcu_bram_addr => spw_bram_addr,
			mcu_bram_di   => spw_bram_din,
			mcu_bram_do   => spw_bram_dout,
			-- cmd
			daq_run  => daq_run,
			cmd_intr => cmd_intr_mcu,
			rst_reg  => rst_reg,
			-- tsp switches
			TSP_EN_A     => TSP_EN_A,
			TSP_EN_D     => TSP_EN_D,
			TSP_EN_BIAS  => TSP_EN_BIAS,
			TSP_pwr_good => TSP_pwr_good,
			-- other flags
			busy         => global_busy,
			SEM_flags    => SEM_flags_sync,
			trig_count   => trig_count,
			busy_viol_cnt=> busy_viol_cnt,
			timestamp    => timestamp,
			cpu_alarms   => cpu_alarms,
			trigger_lut  => trigger_lut
		);

	MCU_inst: BD_CPU
		port map (
			clk_40   => clk_40,
			clk_80   => clk_80,
			reset_rtl   => rst_80,
			axi_rst_out => axi_rst_out,
			cmd_intr_mcu => cmd_intr_mcu,
			-- mcu dpram iface
			spw_bram_clk  => clk_80,
			spw_bram_rst  => rst_80,
			spw_bram_en   => '1',
			spw_bram_we   => spw_bram_we,
			spw_bram_addr => spw_bram_addr,
			spw_bram_din  => spw_bram_din,
			spw_bram_dout => spw_bram_dout,
			-- AXI-lite ctrl module
			AXI_ctrl_AWADDR  => AXI_ctrl_AWADDR,
			AXI_ctrl_AWVALID => AXI_ctrl_AWVALID,
			AXI_ctrl_AWREADY => AXI_ctrl_AWREADY,
			AXI_ctrl_WDATA   => AXI_ctrl_WDATA,
			AXI_ctrl_WSTRB   => AXI_ctrl_WSTRB,
			AXI_ctrl_WVALID  => AXI_ctrl_WVALID,
			AXI_ctrl_WREADY  => AXI_ctrl_WREADY,
			AXI_ctrl_BRESP   => AXI_ctrl_BRESP,
			AXI_ctrl_BVALID  => AXI_ctrl_BVALID,
			AXI_ctrl_BREADY  => AXI_ctrl_BREADY,
			AXI_ctrl_ARADDR  => AXI_ctrl_ARADDR,
			AXI_ctrl_ARVALID => AXI_ctrl_ARVALID,
			AXI_ctrl_ARREADY => AXI_ctrl_ARREADY,
			AXI_ctrl_RDATA   => AXI_ctrl_RDATA,
			AXI_ctrl_RRESP   => AXI_ctrl_RRESP,
			AXI_ctrl_RVALID  => AXI_ctrl_RVALID,
			AXI_ctrl_RREADY  => AXI_ctrl_RREADY,
			-- external SRAM
			EMC_INTF_addr    => EMC_INTF_addr,
			EMC_INTF_adv_ldn => open,
			EMC_INTF_ben(0)  => SRAM_BLE,
			EMC_INTF_ben(1)  => SRAM_BHE,
			EMC_INTF_ce      => open,
			EMC_INTF_ce_n(0) => SRAM_CE,
			EMC_INTF_clken   => open,
			EMC_INTF_cre     => open,
			EMC_INTF_dq_i    => EMC_INTF_dq_i,
			EMC_INTF_dq_o    => EMC_INTF_dq_o,
			EMC_INTF_dq_t    => EMC_INTF_dq_t,
			EMC_INTF_lbon    => open,
			EMC_INTF_oen(0)  => SRAM_OE,
			EMC_INTF_qwen    => open,
			EMC_INTF_rnw     => open,
			EMC_INTF_rpn     => open,
			EMC_INTF_wait    => (others => '0'),
			EMC_INTF_wen     => SRAM_WE,
			-- Pixel data AXI stream
			AXI_STR_PIXDATA_tdata  => axis_pix_tdata,
			AXI_STR_PIXDATA_tlast  => axis_pix_tlast,
			AXI_STR_PIXDATA_tready => axis_pix_tready,
			AXI_STR_PIXDATA_tvalid => axis_pix_tvalid,
			-- QSPI
			qspi_flash_io0_i => qspi_dq_i(0),
			qspi_flash_io0_o => qspi_dq_o(0),
			qspi_flash_io0_t => qspi_dq_t(0),
			qspi_flash_io1_i => qspi_dq_i(1),
			qspi_flash_io1_o => qspi_dq_o(1),
			qspi_flash_io1_t => qspi_dq_t(1),
			qspi_flash_io2_i => qspi_dq_i(2),
			qspi_flash_io2_o => qspi_dq_o(2),
			qspi_flash_io2_t => qspi_dq_t(2),
			qspi_flash_io3_i => qspi_dq_i(3),
			qspi_flash_io3_o => qspi_dq_o(3),
			qspi_flash_io3_t => qspi_dq_t(3),
			qspi_flash_ss_i  => qspi_cs_i,
			qspi_flash_ss_o  => qspi_cs_o,
			qspi_flash_ss_t  => qspi_cs_t,
			qspi_flash_sck_i => qspi_sck_i,
			qspi_flash_sck_o => qspi_sck_o,
			qspi_flash_sck_t => qspi_sck_t,
			owr_i => owr_i,
			owr_o => owr_o,
			owr_t => owr_t
		);


	AXI_ctrl_inst: entity work.AXI_ctrl_regs
		generic map(
			n_staves => n_staves
		)
		port map(
			S_AXI_ACLK    => clk_40,
			S_AXI_ARESETN => axi_rst_out(0),
			S_AXI_AWADDR  => AXI_ctrl_AWADDR,
			S_AXI_AWVALID => AXI_ctrl_AWVALID,
			S_AXI_AWREADY => AXI_ctrl_AWREADY,
			S_AXI_WDATA   => AXI_ctrl_WDATA,
			S_AXI_WSTRB   => AXI_ctrl_WSTRB,
			S_AXI_WVALID  => AXI_ctrl_WVALID,
			S_AXI_WREADY  => AXI_ctrl_WREADY,
			S_AXI_BRESP   => AXI_ctrl_BRESP,
			S_AXI_BVALID  => AXI_ctrl_BVALID,
			S_AXI_BREADY  => AXI_ctrl_BREADY,
			S_AXI_ARADDR  => AXI_ctrl_ARADDR,
			S_AXI_ARVALID => AXI_ctrl_ARVALID,
			S_AXI_ARREADY => AXI_ctrl_ARREADY,
			S_AXI_RDATA   => AXI_ctrl_RDATA,
			S_AXI_RRESP   => AXI_ctrl_RRESP,
			S_AXI_RVALID  => AXI_ctrl_RVALID,
			S_AXI_RREADY  => AXI_ctrl_RREADY,
			-- ALP interface
			alp_cmd       => alp_cmd,
			alp_cmd_valid => alp_cmd_valid,
			alp_cmd_res   => alp_cmd_res,
			alp_cmd_ack   => alp_cmd_ack,
			alp_cmd_res_DV => alp_cmd_res_DV,
			alp_cmd_err   => alp_cmd_err,
			-- Ctrl regs flags
			busy          => busy,
			clk_gate_dis  => clk_gate_dis,
			stave_mask    => stave_mask,
			out_mux_en    => out_mux_en,
			dec_skip_data => dec_skip_data,
			cpu_alarms    => cpu_alarms
		);


	sem_cdc_inst : xpm_cdc_array_single
		generic map (
			DEST_SYNC_FF => 2,
			SRC_INPUT_REG => 0,
			WIDTH => 4
		)
		port map (
			dest_out => SEM_flags_sync,
			dest_clk => clk_80,
			src_clk  => clk_40,
			src_in   => SEM_flags
		);


	qspi_iobuf: for n in 0 to 3 generate
		qspi_dq_IOBUF_inst : IOBUF
			port map(
				O  => qspi_dq_i(n),
				IO => qspi_dq(n),
				I  => qspi_dq_o(n),
				T  => qspi_dq_t(n)
			);
	end generate;

	qspi_ss_IOBUF_inst : IOBUF
		port map(
			O  => qspi_cs_i,
			IO => qspi_cs,
			I  => qspi_cs_o,
			T  => qspi_cs_t
		);

	qspi_sck_IOBUF_inst : IOBUF
	port map(
		O  => qspi_sck_i,
		IO => qspi_sck,
		I  => qspi_sck_o,
		T  => qspi_sck_t
	);

	owm_iob: for n in 0 to n_therm-1 generate
		owm_iob_inst : IOBUF
			port map(
				O  => owr_i(n),
				IO => onewire(n),
				I  => owr_o(n),
				T  => owr_t(n)
			);
	end generate;


	sram_iob: for n in 0 to 15 generate
		sram_iob_inst : IOBUF
			port map(
				O  => EMC_INTF_dq_i(n),
				IO => SRAM_IO(n),
				I  => EMC_INTF_dq_o(n),
				T  => EMC_INTF_dq_t(n)
			);
	end generate;

	SRAM_A <= EMC_INTF_addr(21 downto 1);

end Behavioral;
