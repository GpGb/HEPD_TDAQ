library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

use work.ALPIDE_pkg.all;
use work.types_pkg.all;

entity ALPIDE_FSM is
	generic(
		-- first master id
		master_id  : natural := 16#70#;
		-- number of slaves per maser
		n_slaves : natural := 4;
		-- max allowed length, WARNING:TODO: temporary limit, to tune or connect to reg
		word_count_max : natural := 400
	);
	port(
		clk : in std_logic;
		rst : in std_logic;
		running : out std_logic;
		-- Command interface
		trig    : in std_logic;
		cmd_in  : in std_logic_vector(47 downto 0);
		cmd_DV  : in std_logic;
		cmd_ack : out std_logic;
		res_DV  : out std_logic;
		cmd_err : out std_logic;
		-- readout data output
		data_out      : out std_logic_vector(23 downto 0);
		data_out_strb : out std_logic_vector(2 downto 0);
		data_last     : out std_logic;
		data_valid    : out std_logic;
		buff_full     : in std_logic;
		-- CTRL module interface
		CTRL_start : out std_logic;
		CTRL_done  : in std_logic;
		CTRL_data_in  : out std_logic_vector(47 downto 0);
		CTRL_data_out : in std_logic_vector(15 downto 0);
		RX_err : in std_logic_vector(1 downto 0)
	);
end ALPIDE_FSM;


architecture Behavioral of ALPIDE_FSM is

	type FSM_state is (idle, wait_done, snd_trig, rd_LSB, rd_MSB, check_wrd,
		delay, push_trailer, rst_wait_done, overflow);
	signal state : FSM_state := idle;
	attribute fsm_safe_state : string;
	attribute fsm_safe_state of state : signal is "auto_safe_state";

	constant stave_len : natural := n_slaves*2+1;

	function init_rom return slv8_arr is
		variable tmp : slv8_arr(0 to stave_len);
	begin
		-- first master
		for n in 0 to n_slaves loop
			tmp(n) := uint2slv(master_id + n, 8);
		end loop;
		-- second master
		for n in 0 to n_slaves loop
			tmp(n+n_slaves+1) := uint2slv(master_id + 8 + n, 8);
		end loop;
		return tmp;
	end init_rom;

	constant chip_id_rom : slv8_arr(0 to stave_len) := init_rom;
	signal chipid_ptr, next_chipid_ptr, first_chipid : natural range 0 to stave_len := 0;
	signal chipmask, chip_empty_slv : std_logic_vector(stave_len downto 0) := (others => '0');
	signal word_counter : natural range 0 to word_count_max+1 := 0;
	signal error_counter : natural range 0 to 3 := 0;
	signal idle_counter : natural range 0 to 5 := 0;
	signal counter_trig : std_logic := '0';
	signal rdout_anomaly : std_logic := '0';
	signal trig_cmd_recv : std_logic;
	signal chipmask_nz, stave_dead, single_chip : std_logic := '0';

	type word_enum is (chip_busy_on, chip_busy_off, chip_idle_word, unknown, chip_empty,
		chip_header, region_header, chip_data_short, chip_data_long, chip_trailer);
	signal wordtype : word_enum := chip_empty;
	signal word_strobe : std_logic_vector(2 downto 0);
	signal word_rdvalid : std_logic;

	--attribute mark_debug : string;
	--attribute mark_debug of error_counter, word_counter : signal is "true";
	--attribute mark_debug of data_out, data_valid, data_last, buff_full : signal is "true";
	--attribute mark_debug of state, wordtype, CTRL_done : signal is "true";
	--attribute mark_debug of CTRL_start, CTRL_data_in, RX_err : signal is "true";
	--attribute mark_debug of chipid_ptr, chipmask, chip_empty_slv,	rdout_anomaly : signal is "true";

begin

	process(clk) is
		variable end_of_chain : boolean;
		variable load_next_chip : boolean;
	begin
		if rising_edge(clk) then
			data_valid <= '0';
			data_last <= '0';
			res_DV <= '0';
			cmd_ack <= '0';
			cmd_err <= '0';
			CTRL_start <= '0';
			running <= '1';

			case state is

				when idle =>
					running <= '0';
					chip_empty_slv <= (others => '0');
					word_counter <= 0;

					if stave_dead = '1' and trig = '1' then
						state <= push_trailer;

					elsif trig = '1' then
						CTRL_data_in(OP_code_rng) <= ALPIDE_TRIG;
						CTRL_start <= '1';
						CTRL_data_in(chip_id_rng) <= chip_id_rom(first_chipid);
						chipid_ptr <= first_chipid;
						state <= snd_trig;

					elsif cmd_DV = '1' then
						cmd_ack <= '1';

						case cmd_in(OP_code_rng) is
							when FSM_SET_MSK =>
								chipmask <= cmd_in(stave_len downto 0);
								res_DV <= '1';
							when FSM_RD_MSK =>
								data_out <= (others => '0');
								data_out(stave_len downto 0) <= chipmask;
								res_DV <= '1';
							when ALPIDE_PULSE | ALPIDE_TRIG =>
								trig_cmd_recv <= '1';
								CTRL_start <= '1';
								CTRL_data_in <= cmd_in;
								state <= wait_done;
							when others =>
								CTRL_start <= '1';
								CTRL_data_in <= cmd_in;
								state <= wait_done;
						end case;

					end if;


				when wait_done =>
					if CTRL_done = '1' then
						data_out(15 downto 0) <= CTRL_data_out;
						cmd_err <= or RX_err;
						res_DV <= '1';
						state <= idle;

						if trig_cmd_recv = '1' then
							trig_cmd_recv <= '0';
							CTRL_data_in(OP_code_rng) <= ALPIDE_RDOP;
							CTRL_data_in(chip_id_rng) <= chip_id_rom(first_chipid);
							chipid_ptr <= first_chipid;
							CTRL_data_in(reg_addr_rng) <= ALPIDE_DMU_FIFO_LO;

							if stave_dead = '1' then
								state <= push_trailer;
							else
								state <= rd_LSB;
								CTRL_start <= '1';
							end if;
						end if;

					end if;


				when snd_trig =>
					if CTRL_done = '1' then
						CTRL_start <= '1';
						CTRL_data_in(OP_code_rng) <= ALPIDE_RDOP;
						CTRL_data_in(reg_addr_rng) <= ALPIDE_DMU_FIFO_LO;
						state <= rd_LSB;
					end if;


				when rd_LSB =>
					if CTRL_done = '1' then
						data_out(15 downto 0) <= CTRL_data_out;
						CTRL_start <= '1';
						if buff_full = '1' then
							state <= overflow;
							CTRL_start <= '0';
						else
							state <= rd_MSB;
							CTRL_data_in(OP_code_rng) <= ALPIDE_RDOP;
							CTRL_data_in(reg_addr_rng) <= ALPIDE_DMU_FIFO_HI;
						end if;
					end if;


				when rd_MSB =>

					if CTRL_done = '1' then
						data_out(23 downto 16) <= CTRL_data_out(7 downto 0);

						data_out_strb <= word_strobe;
						data_valid <= word_rdvalid;
						state <= check_wrd;

						if wordtype = chip_empty then
							chip_empty_slv(chipid_ptr) <= '1';
						end if;

						-- error counter
						if wordtype = unknown then
							error_counter <= error_counter + 1;
							if error_counter = 3 then
								counter_trig <= '1';
								chipmask(chipid_ptr) <= '1';
							end if;
						else
							error_counter <= 0;
						end if;

						-- idle counter
						if wordtype = chip_idle_word then
							idle_counter <= idle_counter + 1;
							if idle_counter = 5 then
								counter_trig <= '1';
							end if;
						else
							idle_counter <= 0;
						end if;

						-- word counter
						if word_rdvalid = '1' then
							word_counter <= word_counter + 1;
							if word_counter = word_count_max-1 then
								data_valid <= '0';
							end if;
						end if;

					end if;

				when check_wrd =>
					-- chip busy
					if wordtype = chip_busy_on then
						rdout_anomaly <= '1';
					end if;

					if counter_trig = '1' or (word_counter = word_count_max) then
						data_valid <= '1';
						data_out <= EV_CHIP_SKIPPED;
						data_out_strb <= "100";
						rdout_anomaly <= '1';
						error_counter <= 0;
						idle_counter <= 0;
						counter_trig <= '0';
					end if;

					load_next_chip := (wordtype = chip_trailer or wordtype = chip_empty)
						or counter_trig = '1';
					end_of_chain := (next_chipid_ptr < chipid_ptr) or (single_chip = '1');

					if (end_of_chain and load_next_chip) or (word_counter = word_count_max) then
						CTRL_start <= '0';
						state <= delay;
					else
						CTRL_start <= '1';
						state <= rd_LSB;
					end if;

					CTRL_data_in(OP_code_rng) <= ALPIDE_RDOP;
					CTRL_data_in(reg_addr_rng) <= ALPIDE_DMU_FIFO_LO;
					if load_next_chip then
						chipid_ptr <= next_chipid_ptr;
						CTRL_data_in(chip_id_rng) <= chip_id_rom(next_chipid_ptr);
					end if;

				when delay =>
					-- delay push_trailer write
					state <= push_trailer;

				when push_trailer =>
					data_out <= (others => '0');
					data_out(23 downto 20) <= "1110";
					data_out(19 downto 19-stave_len) <= chipmask;
					data_out(9 downto 9-stave_len) <= chip_empty_slv;
					data_out_strb <= "111";
					data_valid <= '1';
					data_last <= '1';
					if buff_full = '0' then
						if (rdout_anomaly = '1' or chipmask_nz = '1') then
							CTRL_start <= '1';
							CTRL_data_in(OP_code_rng) <= ALPIDE_RORST;
							state <= rst_wait_done;
							rdout_anomaly <= '0';
						else
							state <= idle;
						end if;
					end if;

				when rst_wait_done =>
					if CTRL_done ='1' then
						state <= idle;
					end if;


				when overflow =>
					if buff_full = '0' then
						state <= rd_MSB;
						CTRL_data_in(OP_code_rng) <= ALPIDE_RDOP;
						CTRL_data_in(reg_addr_rng) <= ALPIDE_DMU_FIFO_HI;
						CTRL_start <= '1';
					end if;

			end case;

			if rst = '1' then
				chipmask <= (others => '0');
				state <= idle;
			end if;

		end if;
	end process;


	-- note: next chipid computation ends in max 14 clk's and is needed
	-- before current chipid is updated in rd_MSB state (~100 clk's)
	chipid_gen: process(clk) is
		variable sum : natural range 0 to 11;
	begin
		if rising_edge(clk) then

			-- find first unmasked chip_id and number of masked chips
			first_chipid <= 0;
			sum := 0;
			for i in stave_len downto 0 loop
				if chipmask(i) = '0' then
					first_chipid <= i;
				else
					sum := sum + 1;
				end if;
			end loop;

			chipmask_nz <= '0' when sum = 0 else '1';
			single_chip <= '1' when sum = 9  else '0';
			stave_dead  <= '1' when sum = 10 else '0';

			if single_chip = '1' or stave_dead = '1' then
				next_chipid_ptr <= first_chipid;
			elsif chipmask(next_chipid_ptr) = '1' or chipid_ptr = next_chipid_ptr then
				if next_chipid_ptr = stave_len then
					next_chipid_ptr <= 0;
				else
					next_chipid_ptr <= next_chipid_ptr + 1;
				end if;
			end if;

			if rst = '1' or state = rst_wait_done then
				next_chipid_ptr <= first_chipid;
			end if;

		end if;
	end process;


	word_decode: process(all)
	begin

		if RX_err /= "00" then
			wordtype <= unknown;
			word_strobe <= "100";
			word_rdvalid <= '0';
		elsif CTRL_data_out = x"00ff" then
			wordtype <= chip_idle_word;
			word_strobe <= "100";
			word_rdvalid <= '0';
		elsif CTRL_data_out = x"00f1" then
			wordtype <= chip_busy_on;
			word_strobe <= "000";
			word_rdvalid <= '0';
		elsif CTRL_data_out = x"00f0" then
			wordtype <= chip_busy_off;
			word_strobe <= "000";
			word_rdvalid <= '0';
		elsif CTRL_data_out(15 downto 4) = x"00a" then
			wordtype <= chip_header;
			word_strobe <= "110";
			word_rdvalid <= '1';
		elsif CTRL_data_out(15 downto 4) = x"00b" then
			wordtype <= chip_trailer;
			word_strobe <= "100";
			word_rdvalid <= '1';
		elsif CTRL_data_out(15 downto 4) = x"00e" then
			wordtype <= chip_empty;
			word_strobe <= "110";
			word_rdvalid <= '0';
		elsif CTRL_data_out(15 downto 5) = x"00" & "110" then
			wordtype <= region_header;
			word_strobe <= "100";
			word_rdvalid <= '1';
		elsif CTRL_data_out(15 downto 6) = x"00" & "01" then
			wordtype <= chip_data_short;
			word_strobe <= "110";
			word_rdvalid <= '1';
		elsif CTRL_data_out(15 downto 6) = x"00" & "00" and data_out(7) = '0' then
			wordtype <= chip_data_long;
			word_strobe <= "111";
			word_rdvalid <= '1';
		elsif CTRL_data_out(15 downto 8) /= x"00" then
			wordtype <= unknown;
			word_strobe <= "100";
			word_rdvalid <= '0';
		else
			wordtype <= unknown;
			word_strobe <= "100";
			word_rdvalid <= '0';
		end if;

	end process;

end Behavioral;
