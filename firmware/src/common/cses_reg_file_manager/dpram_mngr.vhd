library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-------------------------------------------------------------------------------
entity dpram_mngr is
	port (
		clk : in std_logic;
		rst : in std_logic;
		-- data input stream
		rx_tdata   : in std_logic_vector(31 downto 0);
		rx_tvalid  : in std_logic;
		rx_tready  : out std_logic;
		rx_tlast   : in std_logic;
		-- regfile read/write interface
		we : in std_logic;
		addr : in std_logic_vector(31 downto 0);
		di : in std_logic_vector(31 downto 0);
		do : out std_logic_vector(31 downto 0);
		-- oob signals
		data_ready : out std_logic;
		data_clear : in std_logic
	);
end dpram_mngr;


architecture Behavioral of dpram_mngr is

	-- dpram iface
	signal dpram_web : std_logic;
	signal dpram_addrb : std_logic_vector(8 downto 0);
	signal dpram_dinb, dpram_doutb : std_logic_vector(31 downto 0);
	signal dly_count : std_logic := '0';

	-- dpram filler FSM
	type FSM_state is (idle, write, write_end, delay, wait_clear);
	signal state : FSM_state := idle;
	attribute fsm_safe_state : string;
	attribute fsm_safe_state of state : signal is "auto_safe_state";

	signal dpram_word_count : unsigned(dpram_addrb'high downto 0) := (others => '0');

	component data_spw_dpram
		port (
			clka   : in std_logic;
			wea    : in std_logic_vector(0 downto 0);
			addra  : in std_logic_vector(8 downto 0);
			dina   : in std_logic_vector(31 downto 0);
			douta  : out std_logic_vector(31 downto 0);
			clkb   : in std_logic;
			web    : in std_logic_vector(0 downto 0);
			addrb  : in std_logic_vector(8 downto 0);
			dinb   : in std_logic_vector(31 downto 0);
			doutb  : out std_logic_vector(31 downto 0)
		);
	end component;


	--attribute mark_debug : string;
	--attribute mark_debug of state, dpram_word_count : signal is "true";
	--attribute mark_debug of rx_tready, rx_tdata, rx_tvalid, rx_tlast : signal is "true";
	--attribute mark_debug of dpram_web, dpram_addrb, dpram_dinb, dpram_doutb : signal is "true";
	--attribute mark_debug of data_ready, data_clear : signal is "true";
	--attribute mark_debug of we, addr, di, do : signal is "true";

begin


	fill_dpram: process(clk)
	begin

		if rising_edge(clk) then

			dpram_web <= '0';
			data_ready <= '0';
			dly_count <= '0';

			case state is

				when idle =>

					dpram_word_count <= (others => '0');
					--ready only when data_clear is '1'
					rx_tready <= data_clear;

					if rx_tvalid = '1' and data_clear = '1' then
						state <= write;
						dpram_word_count <= dpram_word_count + 1;
						dpram_dinb <= rx_tdata;
						dpram_web <= '1';
					end if;

				when write =>

					rx_tready <= '1';
					dpram_dinb <= rx_tdata;

					if rx_tvalid = '1' and dpram_word_count < 2**dpram_word_count'length-1 then
						dpram_web <= rx_tvalid;
						dpram_word_count <= dpram_word_count + 1;
					else
						dpram_web <= '0';
					end if;

					if rx_tlast = '1' then
						state <= write_end;
						rx_tready <= '0';
					end if;

				when write_end =>

					dpram_word_count <= (others => '0');
					dpram_dinb <= (others => '0');
					dpram_dinb(dpram_word_count'range) <= std_logic_vector(dpram_word_count);
					dpram_web <= '1';
					rx_tready <= '0';

					state <= delay;

				when delay =>
					dly_count <= '1';

					if dly_count = '1' then
						state <= wait_clear;
					end if;

				when wait_clear =>

					rx_tready <= '0';
					data_ready <= '1';

					if dpram_doutb = x"0000_0000" and data_clear = '0' then
						state <= idle;
					end if;

			end case;


			if (rst = '1') then

				state <= idle;
				dpram_word_count <= (others => '0');
				dpram_web <= '1';

			end if;

		end if;

	end process fill_dpram;


	dpram_addrb <= std_logic_vector(dpram_word_count);

	data_spw_dpram_inst : data_spw_dpram
	port map (
		clka   => clk,
		wea(0) => we,
		addra  => addr(8 downto 0),
		dina   => di,
		douta  => do,
		clkb   => clk,
		web(0) => dpram_web,
		addrb  => dpram_addrb,
		dinb   => dpram_dinb,
		doutb  => dpram_doutb
	);


end Behavioral;
