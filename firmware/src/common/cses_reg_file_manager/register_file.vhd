library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--use work.hepd_gmx_pkg.all;


entity spw_regfile is

  generic (
    g_spw_addr_width  : integer := 16; --! spw address width generic parameter
    g_spw_data_width  : integer := 32; --! spw data width generic parameter
    g_spw_addr_offset : unsigned := x"0000";     --! component address offset generic parameter
    g_spw_num         : integer := 32 --! spw number generic parameter
  );
  port (
    i_spw_clk     : in std_logic;  --! system clock
    i_rf_clk      : in std_logic;
    i_reset       : in std_logic;  --! master active low reset
    --spw controller interface
    i_data_in     : in  std_logic_vector(g_spw_data_width - 1 downto 0);
    o_data_out    : out std_logic_vector(g_spw_data_width - 1 downto 0);
    i_we          : in  std_logic;
    i_addr        : in  std_logic_vector(g_spw_data_width - 1 downto 0);
    o_write_done  : out std_logic;
    i_busy        : in  std_logic;
    -- FPGA interface
    daq_run       : out std_logic;
    rst_reg       : out std_logic_vector(31 downto 0);
    TSP_EN_A      : out std_logic_vector(14 downto 0);
    TSP_EN_D      : out std_logic_vector(14 downto 0);
    TSP_EN_BIAS   : out std_logic_vector(14 downto 0);
    TSP_pwr_good  : in  std_logic_vector(14 downto 0);
    -- other regs
    busy          : in  std_logic;
    gpio          : in  std_logic_vector(1 downto 0);
    SEM_flags     : in  std_logic_vector(3 downto 0);
    spw_err_cnt   : in  std_logic_vector(15 downto 0);
    trigger_count : in  std_logic_vector(31 downto 0);
    busy_viol_cnt : in  std_logic_vector(15 downto 0);
    timestamp     : in  std_logic_vector(31 downto 0);
    cpu_alarms    : in  std_logic_vector(15 downto 0);
    trigger_lut   : out std_logic_vector(31 downto 0)
  );

end entity spw_regfile;



architecture spw_regfile_arch of spw_regfile is

-------------------------------------------------------------------------------
-- Type Declaration
-------------------------------------------------------------------------------

type register_mode_t is (RW, RO);
type mem_t is array (natural range <>) of std_logic_vector(g_spw_data_width - 1 downto 0);
type addr_t is
  record
      addr        : unsigned(g_spw_addr_width - 1 downto 0);
      modeLocal   : register_mode_t;
      modeRemote  : register_mode_t;
  end record;
type addr_vector_t is array (natural range <>) of addr_t;

-------------------------------------------------------------------------------
-- Function Declaration
-------------------------------------------------------------------------------

function get_local_addr (address : unsigned; address_vector : addr_vector_t) return integer is
begin

    for I in address_vector'range loop

        if (address = address_vector(I).addr) then
            return I;
        end if;

    end loop;

    return address_vector'high;

end function;

-------------------------------------------------------------------------------
-- Constants Definitions
-------------------------------------------------------------------------------
  constant c_register_file_length    : integer := 17;

  constant c_spw_ba                  : unsigned((g_spw_addr_width - 1) downto 0) := g_spw_addr_offset;
  constant c_spw_id_reg_address      : unsigned((g_spw_addr_width - 1) downto 0) := c_spw_ba + to_unsigned(0,g_spw_addr_width);
  constant c_status_reg_address      : unsigned((g_spw_addr_width - 1) downto 0) := c_spw_ba + to_unsigned(1,g_spw_addr_width);
  constant c_reserved_reg_address    : unsigned((g_spw_addr_width - 1) downto 0) := c_spw_ba + to_unsigned(2,g_spw_addr_width);
  constant c_reset_reg_address       : unsigned((g_spw_addr_width - 1) downto 0) := c_spw_ba + to_unsigned(3,g_spw_addr_width);
  constant c_clk_counter_address     : unsigned((g_spw_addr_width - 1) downto 0) := c_spw_ba + to_unsigned(4,g_spw_addr_width);
  constant c_sandbox_address         : unsigned((g_spw_addr_width - 1) downto 0) := c_spw_ba + to_unsigned(5,g_spw_addr_width);
  constant c_reserved1               : unsigned((g_spw_addr_width - 1) downto 0) := c_spw_ba + to_unsigned(6,g_spw_addr_width);
  constant c_reserved2               : unsigned((g_spw_addr_width - 1) downto 0) := c_spw_ba + to_unsigned(7,g_spw_addr_width);
  constant c_version_id              : unsigned((g_spw_addr_width - 1) downto 0) := c_spw_ba + to_unsigned(8,g_spw_addr_width);

  constant c_tdaq_mode_addr          : unsigned((g_spw_addr_width - 1) downto 0) := c_spw_ba + to_unsigned(9,g_spw_addr_width);
  constant c_tsp_en_ad_addr          : unsigned((g_spw_addr_width - 1) downto 0) := c_spw_ba + to_unsigned(10,g_spw_addr_width);
  constant c_tsp_en_bias_addr        : unsigned((g_spw_addr_width - 1) downto 0) := c_spw_ba + to_unsigned(11,g_spw_addr_width);
  constant c_tsp_power_good          : unsigned((g_spw_addr_width - 1) downto 0) := c_spw_ba + to_unsigned(12,g_spw_addr_width);
  constant c_status_b_addr           : unsigned((g_spw_addr_width - 1) downto 0) := c_spw_ba + to_unsigned(13,g_spw_addr_width);
  constant c_trigger_cnt_addr        : unsigned((g_spw_addr_width - 1) downto 0) := c_spw_ba + to_unsigned(14,g_spw_addr_width);
  constant c_timestamp_addr          : unsigned((g_spw_addr_width - 1) downto 0) := c_spw_ba + to_unsigned(15,g_spw_addr_width);
  constant c_trigger_mask            : unsigned((g_spw_addr_width - 1) downto 0) := c_spw_ba + to_unsigned(16,g_spw_addr_width);



  constant address_vector : addr_vector_t(0 to c_register_file_length - 1) := (
      -- control registers
      (addr => c_spw_id_reg_address     , modeLocal => RO, modeRemote => RO), --0
      (addr => c_status_reg_address     , modeLocal => RW, modeRemote => RO), --1
      (addr => c_reserved_reg_address   , modeLocal => RW, modeRemote => RO), --2
      (addr => c_reset_reg_address      , modeLocal => RW, modeRemote => RW), --3
      (addr => c_clk_counter_address    , modeLocal => RW, modeRemote => RO), --4
      (addr => c_sandbox_address        , modeLocal => RW, modeRemote => RW), --5
      (addr => c_reserved1              , modeLocal => RO, modeRemote => RO), --6
      (addr => c_reserved2              , modeLocal => RO, modeRemote => RO), --7
      (addr => c_version_id             , modeLocal => RO, modeRemote => RO), --8

      (addr => c_tdaq_mode_addr         , modeLocal => RW, modeRemote => RW), --9
      (addr => c_tsp_en_ad_addr         , modeLocal => RW, modeRemote => RW), --a
      (addr => c_tsp_en_bias_addr       , modeLocal => RW, modeRemote => RW), --b
      (addr => c_tsp_power_good         , modeLocal => RW, modeRemote => RO), --c
      (addr => c_status_b_addr          , modeLocal => RW, modeRemote => RO), --d
      (addr => c_trigger_cnt_addr       , modeLocal => RW, modeRemote => RO), --e
      (addr => c_timestamp_addr         , modeLocal => RW, modeRemote => RO), --f
      (addr => c_trigger_mask           , modeLocal => RW, modeRemote => Rw) --10
  );

  constant register_vector_reset : mem_t(0 to c_register_file_length - 1) :=
  (
      -- control registers
      x"0F0F0F0F",            -- 0 TDAQ id_reg
      x"00000000",            -- 1 status_reg
      x"00000000",            -- 2 reserved
      x"96969696",            -- 3 reset_reg
      x"00000000",            -- 4 clk_counter
      x"00000000",            -- 5 sandbox
      x"00000000",            -- 6 reserved
      x"00000000",            -- 7 reserved
      x"00000000",            -- 8 reserved
      x"00000000",            -- 9 tdaq_mode
      x"00000000",            -- a tsp_en_ad
      x"00000000",            -- b tsp_en_bias
      x"00000000",            -- c tsp_en_power_good
      x"00000000",            -- d status_b
      x"00000000",            -- e trigger_count
      x"00000000",            -- f timestamp
      x"01041041"             -- 10 trigger_lut
  );

  constant c_sm_busy          : std_logic_vector(31 downto 0) := x"62757379";
  constant c_sm_idle          : std_logic_vector(31 downto 0) := x"69646c65";
  constant c_reset_cmd        : std_logic_vector(31 downto 0) := x"69696969";

-------------------------------------------------------------------------------
-- Signals Definitions
-------------------------------------------------------------------------------
  signal register_vector : mem_t(0 to c_register_file_length - 1);

  -- signals to drive the spw module

  signal s_reset              : std_logic;
  signal s_reset_command_edge : std_logic;
  signal s_reset_command      : std_logic;
  signal s_reset_command_pipe : std_logic;

  signal s_write_done         : std_logic := '0';

  signal s_local_address      : integer;
  signal s_clk_counter        : unsigned(g_spw_data_width-1 downto 0);

  signal r_register_vector    : std_logic_vector(g_spw_data_width - 1 downto 0);
  signal r_write_done         : std_logic;

  --attribute mark_debug: boolean;
  --attribute mark_debug of s_local_address : signal is true;

begin

-------------------------------------------------------------------------------
-- Input assignments
-------------------------------------------------------------------------------

  s_reset           <= i_reset or s_reset_command_edge;
-------------------------------------------------------------------------------
-- Output assignments
-------------------------------------------------------------------------------

  o_write_done      <= r_write_done;
  o_data_out        <= r_register_vector;
-------------------------------------------------------------------------------
-- Internal assignments
-------------------------------------------------------------------------------

  s_reset_command <= '1' when (register_vector(get_local_addr(c_reset_reg_address, address_vector)) = c_reset_cmd) else
                     '0';
  s_reset_command_edge <= s_reset_command and (not s_reset_command_pipe);
  s_local_address <= get_local_addr(unsigned(i_addr), address_vector);
-------------------------------------------------------------------------------
-- Processes declarations
-------------------------------------------------------------------------------

  -----------------------------------------------------------------------------
  --! @brief process to write the control register,
  --!        based on the address written from the cpu
  --!
  --! @param[in]  i_spw_clk        -- system clock
  --! @param[in]  i_reset_n    -- active-low asynchronous reset
  -----------------------------------------------------------------------------


  sample_process: process(i_spw_clk, s_reset)
  begin

    if (s_reset = '1') then

      r_register_vector <= (others=>'0');
      r_write_done <= '0';

    elsif (rising_edge(i_spw_clk)) then

      if to_integer(unsigned(i_addr)) < c_register_file_length then
        r_register_vector <= register_vector(s_local_address);
      else
        case i_addr is
          when (x"00000011") => r_register_vector <= register_vector(1);  -- status;
          when (x"00000012") => r_register_vector <= register_vector(12); -- pwr_foog
          when (x"00000013") => r_register_vector <= register_vector(13); -- status_b
          when (x"00000014") => r_register_vector <= register_vector(14); -- trigger_cnt
          when (x"00000015") => r_register_vector <= register_vector(10); -- tsp_en_ad
          when (x"00000016") => r_register_vector <= register_vector(11); -- tsp_en_bias
          when others   => r_register_vector <= (others => '0');
        end case;
      end if;

      r_write_done <= s_write_done;

    end if;

  end process sample_process;


  regfile_process: process(i_rf_clk, s_reset)

  begin

    if (s_reset = '1') then

      -- reset all registers except the power controls
      for i in 0 to c_register_file_length-1 loop

        --TODO: no magic numbers
        if (i = 16#a# or i = 16#b#) then
            next;
        end if;

        register_vector(i) <= register_vector_reset(i);
      end loop;

      s_write_done <= '0';
      s_clk_counter <= (others => '0');
      s_reset_command_pipe <= '0';

    elsif (rising_edge(i_rf_clk)) then

      s_reset_command_pipe <= s_reset_command;

      s_clk_counter <= s_clk_counter + 1;
      register_vector(get_local_addr(c_clk_counter_address, address_vector)) <= std_logic_vector(s_clk_counter);

      if(i_busy = '0')then

        s_write_done <= '0';

      else

        --registri scritti e letti dalle transizioni spw
        if(i_we = '1')then

          if(address_vector(s_local_address).modeRemote = RW)then

            register_vector(s_local_address) <= i_data_in;

          end if;

          s_write_done <= '1';

        end if;

        -- update RO regs
        register_vector(get_local_addr(c_version_id, address_vector)) <= x"ff00000c";

        register_vector(get_local_addr(c_status_reg_address, address_vector))(15 downto 0) <= cpu_alarms;
        register_vector(get_local_addr(c_status_reg_address, address_vector))(26 downto 25) <= gpio;
        register_vector(get_local_addr(c_status_reg_address, address_vector))(27) <= busy;
        register_vector(get_local_addr(c_status_reg_address, address_vector))(31 downto 28) <= SEM_flags;

        register_vector(get_local_addr(c_tsp_power_good, address_vector))(14 downto 0) <= TSP_pwr_good;

        register_vector(get_local_addr(c_status_b_addr, address_vector))(15 downto 0)  <= spw_err_cnt;
        register_vector(get_local_addr(c_status_b_addr, address_vector))(31 downto 16)  <= busy_viol_cnt;

        register_vector(get_local_addr(c_trigger_cnt_addr, address_vector)) <= trigger_count;
        register_vector(get_local_addr(c_timestamp_addr, address_vector)) <= timestamp;

      end if;


      -- clear reset register
      if register_vector(get_local_addr(c_reset_reg_address, address_vector))
        /= register_vector_reset(get_local_addr(c_reset_reg_address, address_vector)) then

          register_vector(get_local_addr(c_reset_reg_address, address_vector)) <=
              register_vector_reset(get_local_addr(c_reset_reg_address, address_vector));
      end if;

    end if;

  end process regfile_process;


  rst_reg <= register_vector(get_local_addr(c_reset_reg_address, address_vector));

  -- tdaq mode
  daq_run <= '1' when register_vector(get_local_addr(c_tdaq_mode_addr, address_vector))
                      /= x"0000_0000" else '0';

  trigger_lut <= register_vector(get_local_addr(c_trigger_mask, address_vector));

  -- assign power control regs
  TSP_EN_A     <= register_vector(get_local_addr(c_tsp_en_ad_addr, address_vector))(14 downto 0);
  TSP_EN_D     <= register_vector(get_local_addr(c_tsp_en_ad_addr, address_vector))(30 downto 16);
  TSP_EN_BIAS  <= register_vector(get_local_addr(c_tsp_en_bias_addr, address_vector))(14 downto 0);


end architecture spw_regfile_arch;
