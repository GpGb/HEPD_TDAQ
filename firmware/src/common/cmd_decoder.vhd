library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

use work.types_pkg.all;

Library xpm;
use xpm.vcomponents.all;


entity cmd_decoder is
	port(
		clk_alp  : in std_logic;
		clk_pkgr : in std_logic;
		ext_rst  : in std_logic;
		rst_reg  : in std_logic_vector(31 downto 0);
		rst_80   : out std_logic := '1';
		rst_daq  : out std_logic;
		rst_time : out std_logic;
		-- trigger
		daq_run   : in std_logic;
		busy      : in std_logic;
		trig_evnt : in std_logic;
		trig_in   : in std_logic_vector(4 downto 0);
		trig_out  : out std_logic_vector(4 downto 0);
		trig_mask : out std_logic_vector(4 downto 0);
		trig_mask_ev  : out std_logic;
		trig_cnt_out  : out std_logic_vector(31 downto 0);
		busy_viol_cnt : out std_logic_vector(15 downto 0);
		trigger_lut   : in std_logic_vector(31 downto 0)
	);
end entity;


architecture Behavioral of cmd_decoder is

	constant reset_global_cmd : std_logic_vector(31 downto 0) := x"55555555";
	constant reset_daq_cmd    : std_logic_vector(31 downto 0) := x"0da00da0";
	constant reset_time_cmd   : std_logic_vector(31 downto 0) := x"00000000";
	constant reset_default    : std_logic_vector(31 downto 0) := x"96969696";

	-- trigger management
	signal master_trig, trig_pedge, trig_evnt_sync : std_logic := '0';
	signal trig_reg : std_logic_vector(1 downto 0) := "00";
	signal trig_in_sync, trig_in_reg : std_logic_vector(4 downto 0);
	signal busy_violation_count : unsigned(15 downto 0) := (others => '0');
	signal trig_count : unsigned(31 downto 0) := (others => '0');

	signal daq_run_sync : std_logic := '0';

	-- soft rst command detection
	signal rst_daq_80 : std_logic := '1';
	signal reset_sr : std_logic_vector(31 downto 0) := (others => '0');

	--attribute mark_debug : string;
	--attribute mark_debug of trig_in_reg, trig_in_sync, trig_out : signal is "true";
	--attribute mark_debug of master_trig, trig_reg : signal is "true";

begin

	trig_ev_cdc_inst : xpm_cdc_sync_rst
		generic map(
			DEST_SYNC_FF => 2,
			INIT => 0
		)
		port map (
			dest_rst => trig_evnt_sync,
			dest_clk => clk_alp,
			src_rst => trig_evnt
	);

	trig_in_cdc_inst : xpm_cdc_array_single
		generic map (
			DEST_SYNC_FF => 2,
			SRC_INPUT_REG => 0,
			WIDTH => 5
		)
		port map (
			dest_out => trig_in_sync,
			dest_clk => clk_alp,
			src_clk  => '0',
			src_in => trig_in
		);

	-- trigger rising edge detection and counter
	sync_trig: process(clk_alp)
	begin
		if rising_edge(clk_alp) then

			-- generate trigger pulse and increase counter
			trig_out <= (others => '0');

			if master_trig = '1' then
				trig_count <= trig_count + 1;

				for i in 0 to trig_out'length-1 loop
					trig_out(i) <= or (trig_in_reg and trigger_lut(5*i+4 downto 5*i)) or trigger_lut(31);
				end loop;

			end if;


			-- reset trig counter
			if rst_daq = '1' then
				trig_count <= (others => '0');
			end if;


			-- trig_evnt pedge detection
			trig_reg <= trig_evnt_sync & trig_reg(1);
			master_trig <= '0';

			if trig_pedge = '1' and busy = '0' and daq_run_sync = '1' then
				master_trig <= '1';
				trig_in_reg <= trig_in_sync;
			end if;

		end if;
	end process;

	trig_pedge <= (not trig_reg(0)) and trig_reg(1);

	trig_cnt_out <= std_logic_vector(trig_count);


	-- DEST_SYNC_FF must be trig_event_cdc.DEST_SYNC_FF
	-- TODO: bad idea?
	trig_mask_cdc2_inst : xpm_cdc_array_single
		generic map (
			DEST_SYNC_FF  => 2,
			SRC_INPUT_REG => 0,
			WIDTH => 5
		)
		port map (
			dest_out => trig_mask,
			dest_clk => clk_pkgr,
			src_clk  => '0',
			src_in   => trig_out
		);

	trig_event_cdc : xpm_cdc_pulse
		generic map(
			DEST_SYNC_FF => 2,
			INIT_SYNC_FF => 0,
			REG_OUTPUT   => 0,
			RST_USED     => 0
		)
		port map(
			dest_pulse => trig_mask_ev,
			dest_clk   => clk_pkgr,
			dest_rst   => '0',
			src_clk    => clk_alp,
			src_pulse  => master_trig,
			src_rst    => rst_daq
		);


	busy_violation_cnt: process(clk_alp)
	begin
		if rising_edge(clk_alp) then

			if (trig_pedge and busy) = '1' then
				busy_violation_count <= busy_violation_count + 1;
			end if;

			if rst_daq = '1' then
				busy_violation_count <= (others => '0');
			end if;

		end if;
	end process;
	busy_viol_cnt <= std_logic_vector(busy_violation_count);


	-- reset manager
	rst_ctrl: process(clk_pkgr)
	begin
		if rising_edge(clk_pkgr) then

			if reset_sr(0) = '1' then
				rst_daq_80 <= '0';
				rst_80 <= '0';
			end if;

			reset_sr <= '1' & reset_sr(31 downto 1);
			rst_time <= '0';

			if ext_rst = '0' or rst_reg = reset_global_cmd then
				reset_sr <= (others => '0');
				rst_daq_80 <= '1';
				rst_80 <= '1';
			elsif rst_reg = reset_daq_cmd then
				reset_sr <= (others => '0');
				rst_daq_80 <= '1';
			elsif rst_reg = reset_time_cmd then
				rst_time <= '1';
			end if;

		end if;
	end process;


	rst_daq_cdc : xpm_cdc_sync_rst
		generic map(
			DEST_SYNC_FF => 2,
			INIT => 1,
			INIT_SYNC_FF => 0,
			SIM_ASSERT_CHK => 0
		)
		port map (
			dest_rst => rst_daq,
			dest_clk => clk_alp,
			src_rst  => rst_daq_80
		);


	cdc_daq_run : xpm_cdc_single
	generic map(
		DEST_SYNC_FF => 2,
		INIT_SYNC_FF => 0
	)
	port map(
		dest_out => daq_run_sync,
		dest_clk => clk_alp,
		src_clk  => clk_pkgr,
		src_in   => daq_run
	);


end Behavioral;
