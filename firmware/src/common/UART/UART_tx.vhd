library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- This is going to be connected directly to the output of FIFO: 
-- Din => FIFO_data_out, DV => not FIFO_empy, rd_rq => fifo_rd
-- i.e. it will continue transmitting until fifo empty.

entity UART_Tx is
  -- NOTE how we always work with CLK_per_BAUD - 2:
  -- -1 because counting start from 0
  -- -1 for the 1 clk delay during state change.
  generic(CLK_per_BAUD: natural := 25);

  port( 
    CLK   : in std_logic;
    CTS   : in std_logic;
    DV    : in std_logic;
    Din   : in std_logic_vector(7 downto 0);
    rd_rq : out std_logic;
    S_Tx  : out std_logic
  );

end UART_Tx;


architecture transmitter of UART_Tx is

  type UART_state is (idle, Tx_start_bit, Tx_data_bit, Tx_stop_bit);

  signal curr_state : UART_state := idle;
  signal counter    : natural range 0 to CLK_per_BAUD - 2 := 0;
  
  signal data_in    : std_logic_vector(7 downto 0);
  signal dat_index  : natural range 0 to 8 := 0;
  signal bit_out    : std_logic := '1';

begin

  S_Tx <= bit_out;

  Trs_SM: process(CLK)
  begin
  
    if rising_edge(CLK) then
      
      --default values
      rd_rq <= '0';
      case curr_state is
      
        when idle =>
          counter <= 0;
          bit_out <= '1';
          if DV = '1' and CTS = '0' then
            curr_state <= Tx_start_bit;
          else
            curr_state <= idle;
          end if;


        when Tx_start_bit =>
          bit_out <= '0';
          if counter < CLK_per_BAUD - 2 then
            counter <= counter + 1;
            curr_state <= Tx_start_bit;
          else
            data_in <= Din;
            rd_rq <= '1';
            counter <= 0;
            curr_state <= Tx_data_bit;
          end if;


        when Tx_data_bit =>
          bit_out <= data_in(0);
          if counter < CLK_per_BAUD - 2 then
            counter <= counter + 1;
            curr_state <= Tx_data_bit;
          else
            counter <= 0;
				    data_in <= data_in(0) & data_in(7 downto 1);
            if dat_index = 7 then
              dat_index <= 0;
              curr_state <= Tx_stop_bit;
				    else
				      dat_index <= dat_index + 1;
            end if;
			    end if;


        when Tx_stop_bit =>
          bit_out <= '1';
          if counter < CLK_per_BAUD - 2 then
            counter <= counter + 1;
          else
            counter <= 0;
            curr_state <= idle;
          end if;

      end case;
      
    end if;
 
  end process Trs_SM;

end transmitter;
