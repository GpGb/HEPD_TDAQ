library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;
Library UNISIM;
use UNISIM.vcomponents.all;

entity SEM is
	port(
		clk : in std_logic;
		SEM_flags : out std_logic_vector(3 downto 0);
		-- uart mon shim
		monitor_tx : out std_logic;
		monitor_rx : in std_logic
	);
end SEM;


architecture Behavioral of SEM is

	signal status_heartbeat, status_initialization, status_observation, status_correction,
		status_classification, status_injection, status_essential, status_uncorrectable : std_logic;

	signal icap_csib, icap_rdwrb : std_logic;
	signal icap_o, icap_i : std_logic_vector(31 downto 0);

	signal fecc_crcerr, fecc_eccerr, fecc_eccerrsingle, fecc_syndromevalid : std_logic;
	signal fecc_syndrome : std_logic_vector(12 downto 0);
	signal fecc_far : std_logic_vector(25 downto 0);
	signal fecc_synbit : std_logic_vector(4 downto 0);
	signal fecc_synword : std_logic_vector(6 downto 0);

	signal beat_cnt : integer range 0 to 150 := 0;
	signal heart_fail : std_logic := '0';
	signal status_arr : std_logic_vector(4 downto 0);
	signal critical_fail, status_idle : std_logic := '0';

	signal monitor_txdata, monitor_rxdata : std_logic_vector(7 downto 0);
	signal monitor_rxempty, monitor_rxread, monitor_txwrite, monitor_txfull : std_logic;
	signal uart_out_DV : std_logic;

	--attribute mark_debug : string;
	--attribute mark_debug of monitor_txdata, monitor_txwrite, monitor_txfull, monitor_tx : signal is "true";
	--attribute mark_debug of status_arr, SEM_flags : signal is "true";


	component sem_0
		port (
			status_heartbeat : out std_logic;
			status_initialization : out std_logic;
			status_observation : out std_logic;
			status_correction : out std_logic;
			status_classification : out std_logic;
			status_injection : out std_logic;
			status_essential : out std_logic;
			status_uncorrectable : out std_logic;
			monitor_txdata : out std_logic_vector(7 downto 0);
			monitor_txwrite : out std_logic;
			monitor_txfull : in std_logic;
			monitor_rxdata : in std_logic_vector(7 downto 0);
			monitor_rxread : out std_logic;
			monitor_rxempty : in std_logic;
			icap_o : in std_logic_vector(31 downto 0);
			icap_csib : out std_logic;
			icap_rdwrb : out std_logic;
			icap_i : out std_logic_vector(31 downto 0);
			icap_clk : in std_logic;
			icap_request : out std_logic;
			icap_grant : in std_logic;
			fecc_crcerr : in std_logic;
			fecc_eccerr : in std_logic;
			fecc_eccerrsingle : in std_logic;
			fecc_syndromevalid : in std_logic;
			fecc_syndrome : in std_logic_vector(12 downto 0);
			fecc_far : in std_logic_vector(25 downto 0);
			fecc_synbit : in std_logic_vector(4 downto 0);
			fecc_synword : in std_logic_vector(6 downto 0)
		);
	end component;

begin

	status_arr(0) <= status_initialization;
	status_arr(1) <= status_observation;
	status_arr(2) <= status_correction;
	status_arr(3) <= status_classification;
	status_arr(4) <= status_injection;

	critical_fail <= heart_fail or status_uncorrectable or (and status_arr);
	status_idle <= nor status_arr;

	SEM_flags(0) <= critical_fail;
	SEM_flags(1) <= status_uncorrectable;
	SEM_flags(2) <= status_idle;
	SEM_flags(3) <= status_observation;


	sem_inst: sem_0
		port map(
			status_heartbeat      => status_heartbeat,
			status_initialization => status_initialization,
			status_observation    => status_observation,
			status_correction     => status_correction,
			status_classification => status_classification,
			status_injection      => status_injection,
			status_essential      => status_essential, -- can ignore with current conf
			status_uncorrectable  => status_uncorrectable,
			monitor_txdata        => monitor_txdata,
			monitor_txwrite       => monitor_txwrite,
			monitor_txfull        => monitor_txfull,
			monitor_rxdata        => monitor_rxdata,
			monitor_rxread        => monitor_rxread,
			monitor_rxempty       => monitor_rxempty,
			icap_o       => icap_o,
			icap_csib    => icap_csib,
			icap_rdwrb   => icap_rdwrb,
			icap_i       => icap_i,
			icap_clk     => clk,
			icap_grant   => '1',
			icap_request => open,
			fecc_crcerr        => fecc_crcerr,
			fecc_eccerr        => fecc_eccerr,
			fecc_eccerrsingle  => fecc_eccerrsingle,
			fecc_syndromevalid => fecc_syndromevalid,
			fecc_syndrome      => fecc_syndrome,
			fecc_far           => fecc_far,
			fecc_synbit        => fecc_synbit,
			fecc_synword       => fecc_synword
		);


	ICAPE2_inst : ICAPE2
		generic map(
			DEVICE_ID => X"03651093",
			ICAP_WIDTH => "X32",
			SIM_CFG_FILE_NAME => "NONE"
		)
		port map(
			O     => icap_o,
			CLK   => clk,
			CSIB  => icap_csib,
			I     => icap_i,
			RDWRB => icap_rdwrb
		);


	FRAME_ECCE2_inst : FRAME_ECCE2
		generic map(
			FARSRC => "EFAR",
			FRAME_RBT_IN_FILENAME => "NONE"
		)
		port map(
			CRCERROR      => fecc_crcerr,
			ECCERROR      => fecc_eccerr,
			ECCERRORSINGLE => fecc_eccerrsingle,
			FAR           => fecc_far,
			SYNBIT        => fecc_synbit,
			SYNDROME      => fecc_syndrome,
			SYNDROMEVALID => fecc_syndromevalid,
			SYNWORD       => fecc_synword
		);


	heartbt_chk: process(clk)
	begin
		if rising_edge(clk) then
			if status_observation = '1' then

				if status_heartbeat = '1' then
					beat_cnt <= 0;
					heart_fail <= '0';
				elsif beat_cnt = 150 then
					heart_fail <= '1';
				else
					beat_cnt <= beat_cnt + 1;
				end if;

			end if;
		end if;
	end process;


	mon_shim_uart: entity work.UART_controller
		port map(
			CLK   => clk,
			Tx    => monitor_tx,
			Rx    => monitor_rx,
			CTS   => '0',
			RTS   => open,
			Data_in  => monitor_txdata,
			Data_out => monitor_rxdata,
			in_DV    => monitor_txwrite,
			out_rd   => monitor_rxread,
			out_DV   => uart_out_DV,
			busy     => monitor_txfull
		);
	monitor_rxempty <= not uart_out_DV;

end Behavioral;
