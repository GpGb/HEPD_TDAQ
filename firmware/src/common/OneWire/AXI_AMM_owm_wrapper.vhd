library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.types_pkg.all;

entity AXI_owm_wrapper is
	generic (
		C_S_AXI_DATA_WIDTH	: integer	:= 32;
		C_S_AXI_ADDR_WIDTH	: integer	:= 32;
		OWN_ports : integer := 16
	);
	port (
		S_AXI_ACLK : in std_logic;
		S_AXI_ARESETN : in std_logic;
		-- AXI Write channel
		S_AXI_AWADDR  : in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		S_AXI_AWVALID : in std_logic;
		S_AXI_AWREADY : out std_logic;
		S_AXI_WDATA   : in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		S_AXI_WVALID  : in std_logic;
		S_AXI_WREADY  : out std_logic;
		S_AXI_BRESP   : out std_logic_vector(1 downto 0);
		S_AXI_BVALID  : out std_logic;
		S_AXI_BREADY  : in std_logic;
		-- AXI Read channel
		S_AXI_ARADDR  : in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		S_AXI_ARVALID : in std_logic;
		S_AXI_ARREADY : out std_logic;
		S_AXI_RDATA   : out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		S_AXI_RRESP   : out std_logic_vector(1 downto 0);
		S_AXI_RVALID  : out std_logic;
		S_AXI_RREADY  : in std_logic;
		-- One-Wire buffer signals
		owr_i : in std_logic_vector(OWN_ports-1 downto 0);
		owr_o : out std_logic_vector(OWN_ports-1 downto 0);
		owr_t : out std_logic_vector(OWN_ports-1 downto 0)
	);
end AXI_owm_wrapper;

architecture arch_imp of AXI_owm_wrapper is

	constant ADDR_LSB  : integer := (C_S_AXI_DATA_WIDTH/32)+ 1;
	constant OPT_MEM_ADDR_BITS : integer := 1;
	constant MAX_REG_LEN : integer := 2;

	signal axi_bvalid, axi_arready, axi_rvalid : std_logic;
	signal aw_en, rd_en : std_logic;

	-- OneWire module
	signal rst, bus_ren, bus_wen : std_logic;
	signal bus_adr, bus_adr_w, bus_adr_r : std_logic_vector(0 downto 0);
	signal bus_wdt, bus_rdt : std_logic_vector(31 downto 0);
	signal m_owr_p, m_owr_e, m_owr_i : std_logic_vector(OWN_ports-1 downto 0);

	component sockit_owm
		generic(OWN : integer);
		port(
			clk : in std_logic;
			rst : in std_logic;
			bus_ren : in std_logic;
			bus_wen : in std_logic;
			bus_adr : in std_logic_vector(0 downto 0);
			bus_wdt : in std_logic_vector(31 downto 0);
			bus_rdt : out std_logic_vector(31 downto 0);
			bus_irq : out std_logic;
			owr_p : out std_logic_vector(OWN_ports-1 downto 0);
			owr_e : out std_logic_vector(OWN_ports-1 downto 0);
			owr_i : in std_logic_vector(OWN_ports-1 downto 0)
		);
	end component sockit_owm;

begin

	-- AXI Write management
	-- The process waits for AWVALID, WVALID and then assert AWREADY and WREADY.
	-- Then then response is generated and and BVALID is asserted.
	-- In this case the write data is accepted (address is valid), data is written
	-- to memory mapped registers.

	-- The process waits for BREADY to deassert BVALID and restart the cycle.

	S_AXI_BVALID <= axi_bvalid;

	process (S_AXI_ACLK)

		variable loc_addr : integer;

	begin

		if rising_edge(S_AXI_ACLK) then

			if S_AXI_ARESETN = '0' then

				axi_bvalid  <= '0';
				S_AXI_BRESP <= "00";
				aw_en <= '0';

			else

				loc_addr := slv2uint(S_AXI_AWADDR(ADDR_LSB + OPT_MEM_ADDR_BITS downto ADDR_LSB));

				if ((S_AXI_AWVALID and S_AXI_WVALID) = '1' and aw_en = '0') then

					if loc_addr < MAX_REG_LEN then
						-- write data
						bus_wdt <= S_AXI_WDATA;
						bus_adr_w(0) <= S_AXI_AWADDR(2); -- 2 reg addrs, word-addressing
						aw_en <= '1';
						-- "OKAY" resp
						S_AXI_BRESP  <= "00";
					else
						-- "SLVERR" resp
						S_AXI_BRESP  <= "10";
					end if;

					S_AXI_AWREADY <= '1';
					S_AXI_WREADY <= '1';

				else
					S_AXI_AWREADY <= '0';
					S_AXI_WREADY <= '0';
				end if;


				if aw_en = '1' then
					axi_bvalid <= '1';
				end if;


				if S_AXI_BREADY = '1' and axi_bvalid = '1' then
						aw_en <= '0';
						axi_bvalid <= '0';
				end if;

			end if;
		end if;

	end process;


	-- AXI read management
	-- ARREADY is assrted when ARVALID is asserted.
	-- When ARVALID is asserted a response is generated and RVALID is asserted until RREADY is asserted.
	-- The read address is read and corresponding register is selected and served on the axi bus.

	S_AXI_ARREADY <= axi_arready;
	S_AXI_RVALID <= axi_rvalid;

	process (S_AXI_ACLK)

		variable loc_addr : integer;

	begin

		if rising_edge(S_AXI_ACLK) then

			loc_addr := slv2uint(S_AXI_ARADDR(ADDR_LSB + OPT_MEM_ADDR_BITS downto ADDR_LSB));

			if S_AXI_ARESETN = '0' then

				axi_arready <= '0';
				axi_rvalid  <= '0';
				S_AXI_RRESP   <= "00";
				S_AXI_RDATA <= (others => '0');
				rd_en <= '0';

			else

				axi_arready <= rd_en;

				-- accept read address
				if (S_AXI_ARVALID = '1') then
					axi_arready <= '1';
					rd_en <= '1';
					bus_adr_r(0) <= S_AXI_ARADDR(2); -- 2 reg addrs, word-addressing
				end if;


				-- generate read response (DATA + RRESP)
				if (axi_arready and S_AXI_ARVALID) = '1' then

					axi_rvalid <= '1';
					S_AXI_RDATA <= bus_rdt;

					if loc_addr < MAX_REG_LEN then
						S_AXI_RRESP  <= "00"; -- 'OKAY' response
					else
						S_AXI_RRESP  <= "10"; -- 'SLVERR' response
					end if;

				end if;


				-- read complete when RREADY is asserted
				if (S_AXI_RREADY and axi_rvalid) = '1' then
					rd_en <= '0';
					axi_rvalid <= '0';
				end if;


			end if;
		end if;

	end process;


	---------------------- One-Wire module ------------------------

	rst <= not S_AXI_ARESETN;
	bus_adr <= bus_adr_w when aw_en = '1' else bus_adr_r;
	bus_wen <= aw_en;
	bus_ren <= rd_en;

	owm_inst: sockit_owm
	generic map(OWN => OWN_ports)
	port map(
		clk => S_AXI_ACLK,
		rst => rst,
		bus_ren => bus_ren,
		bus_wen => bus_wen,
		bus_adr => bus_adr,
		bus_wdt => bus_wdt,
		bus_rdt => bus_rdt,
		bus_irq => open,
		owr_p => m_owr_p,
		owr_e => m_owr_e,
		owr_i => m_owr_i
	);

	m_owr_i <= owr_i;
	owr_o <= m_owr_p;
	owr_t <= not (m_owr_p or m_owr_e);

end arch_imp;
