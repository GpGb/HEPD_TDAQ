library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library UNISIM;
use UNISIM.VComponents.all;


entity spw_axis_wrapper is
	port(
		clk : in std_logic;
		rst : in std_logic;
		rst_daq : in std_logic;
		-- spw pins
		FT_CLK   : in std_logic;
		FT_DATA  : inout std_logic_vector(7 downto 0);
		FT_RXF_N : in std_logic;
		FT_TXE_N : in std_logic;
		FT_OE_N  : out std_logic;
		FT_RD_N  : out std_logic;
		FT_WR_N  : out std_logic;
		-- gpio spw
		event_ready : out std_logic;
		event_clear : in std_logic;
		-- data AXIS interface
		pkgr_data  : in std_logic_vector(31 downto 0);
		pkgr_valid : in std_logic;
		pkgr_ready : out std_logic;
		pkgr_last  : in std_logic;
		-- MCU dpram iface
		mcu_bram_we   : out std_logic_vector(3 downto 0);
		mcu_bram_addr : out std_logic_vector(31 downto 0);
		mcu_bram_di   : out std_logic_vector(31 downto 0);
		mcu_bram_do   : in std_logic_vector(31 downto 0);
		-- cmd
		daq_run  : out std_logic;
		cmd_intr : out std_logic;
		rst_reg  : out std_logic_vector(31 downto 0);
		-- TSP switches
		TSP_EN_A     : out std_logic_vector(14 downto 0);
		TSP_EN_D     : out std_logic_vector(14 downto 0);
		TSP_EN_BIAS  : out std_logic_vector(14 downto 0);
		TSP_pwr_good : in std_logic_vector(14 downto 0);
		-- other regs
		busy         : in std_logic;
		SEM_flags    : in std_logic_vector(3 downto 0);
		trig_count   : in std_logic_vector(31 downto 0);
		busy_viol_cnt: in std_logic_vector(15 downto 0);
		timestamp    : in std_logic_vector(31 downto 0);
		cpu_alarms   : in std_logic_vector(15 downto 0);
		trigger_lut  : out std_logic_vector(31 downto 0)
	);
end spw_axis_wrapper;


architecture wrap_bhv of spw_axis_wrapper is

	constant cmd_addr : std_logic_vector := x"0000_2000";

	signal clk_ftdi : std_logic;

	-- FT rx buffer
	signal s_ft_rx_tvalid, s_ft_rx_tready, s_ft_rx_tlast : std_logic;
	signal m_ft_rx_tvalid, m_ft_rx_tready, m_ft_rx_tlast : std_logic;
	signal s_ft_rx_tdata, m_ft_rx_tdata : std_logic_vector(7 downto 0);

	-- FT tx buffer
	signal s_ft_tx_tvalid, s_ft_tx_tready, s_ft_tx_tlast : std_logic;
	signal m_ft_tx_tvalid, m_ft_tx_tready : std_logic;
	signal s_ft_tx_tdata, m_ft_tx_tdata : std_logic_vector(7 downto 0);

	signal rxcount : natural range 0 to 10 := 0;

	-- spw controller
	signal s_data_in, s_data_out : std_logic_vector(31 downto 0);
	signal s_addr : std_logic_vector(31 downto 0);
	signal s_we, s_write_done, s_busy : std_logic;

	signal spw_txrdy, spw_txflag, spw_txwrite : std_logic;
	signal spw_txdata : std_logic_vector(7 downto 0);

	signal regfile_data_in, regfile_data_out : std_logic_vector(31 downto 0);
	signal regfile_addr : std_logic_vector(31 downto 0);
	signal regfile_we, regfile_write_done : std_logic;

	signal pkgr8_data : std_logic_vector(7 downto 0);
	signal pkgr8_valid, pkgr8_ready, pkgr8_last : std_logic;


	constant g_spw_data_width : natural := 32;

	component spw_controller
		generic(
			g_spw_addr_width  : integer := 16;
			g_spw_data_width  : integer := 32;
			g_spw_addr_offset : unsigned := x"0000";
			g_spw_num         : integer := 32;
			g_spw_idx         : unsigned(7 downto 0) := x"00"
		);
		port (
			i_spw_clk     : in  std_logic;
			i_reset       : in  std_logic;
			--regfile inte
			i_data_in     : in  std_logic_vector(g_spw_data_width - 1 downto 0);
			o_data_out    : out std_logic_vector(g_spw_data_width - 1 downto 0);
			o_we          : out std_logic;
			o_addr        : out std_logic_vector(g_spw_data_width - 1 downto 0);
			i_write_done  : in  std_logic;
			o_busy        : out std_logic;
			--SPW interfac
			i_txrdy       : in  std_logic;
			i_rxvalid     : in  std_logic;
			i_rxflag      : in  std_logic;
			i_rxdata      : in  std_logic_vector(7 downto 0);
			o_rxread      : out std_logic;
			o_txwrite     : out std_logic;
			o_txflag      : out std_logic;
			o_txdata      : out std_logic_vector(7 downto 0)
		);
	end component spw_controller;


	--attribute mark_debug : string;
	--attribute mark_debug of cmd_intr : signal is "true";
	--attribute mark_debug of s_data_in, s_data_out, s_addr, s_we : signal is "true";
	--attribute mark_debug of pkgr8_valid, pkgr8_ready, pkgr8_data, pkgr8_last : signal is "true";
	--attribute mark_debug of s_write_done, s_busy  signal is "true";

begin

	register_file_unit : entity work.spw_regfile
	generic map(
		g_spw_addr_width  => 20,   -- spw address width generic parameter
		g_spw_data_width  => 32,   -- spw data width generic parameter
		g_spw_addr_offset => x"00000",
		g_spw_num         => 32    -- spw number generic parameter
	)
	port map (
		i_spw_clk         => clk,
		i_rf_clk          => clk,
		i_reset           => rst,
		-- controller interface
		i_data_in         => regfile_data_in,
		o_data_out        => regfile_data_out,
		i_we              => regfile_we,
		i_addr            => regfile_addr,
		o_write_done      => regfile_write_done,
		i_busy            => s_busy,
		-- FPGA interface
		daq_run           => daq_run,
		rst_reg           => rst_reg,
		TSP_EN_A          => TSP_EN_A,
		TSP_EN_D          => TSP_EN_D,
		TSP_EN_BIAS       => TSP_EN_BIAS,
		TSP_pwr_good      => TSP_pwr_good,
		busy              => busy,
		gpio              => event_ready & event_clear,
		SEM_flags         => SEM_flags,
		spw_err_cnt       => (others => '0'),
		trigger_count     => trig_count,
		busy_viol_cnt     => busy_viol_cnt,
		timestamp         => timestamp,
		cpu_alarms        => cpu_alarms,
		trigger_lut       => trigger_lut
	);

	spw_controller_inste: spw_controller
	generic map (
		g_spw_addr_width   => 20,    -- spw address width generic parameter
		g_spw_data_width   => 32,    -- spw data width generic parameter
		g_spw_addr_offset  => x"0000",
		g_spw_num          => 32,    -- spw number generic parameter
		g_spw_idx          => x"00"  -- unique ID index generic parameter
	)
	port map (
		i_spw_clk          => clk,
		i_reset            => rst,
		--regfile interface
		i_data_in          => s_data_out,
		o_data_out         => s_data_in,
		o_we               => s_we,
		o_addr             => s_addr,
		i_write_done       => s_write_done,
		o_busy             => s_busy,
		--SPW interface
		i_txrdy            => spw_txrdy,
		i_rxvalid          => m_ft_rx_tvalid,
		i_rxflag           => m_ft_rx_tlast,
		i_rxdata           => m_ft_rx_tdata,
		o_rxread           => m_ft_rx_tready,
		o_txwrite          => spw_txwrite,
		o_txflag           => spw_txflag,
		o_txdata           => spw_txdata
	);

	FT_rx_fifo_inst : entity work.FT_data_FIFO
	port map(
		s_axis_aresetn => not rst,
		s_axis_aclk    => clk_ftdi,
		m_axis_aclk    => clk,
		s_axis_tvalid  => s_ft_rx_tvalid,
		s_axis_tready  => s_ft_rx_tready,
		s_axis_tdata   => s_ft_rx_tdata,
		s_axis_tlast   => s_ft_rx_tlast,
		m_axis_tvalid  => m_ft_rx_tvalid,
		m_axis_tready  => m_ft_rx_tready,
		m_axis_tdata   => m_ft_rx_tdata,
		m_axis_tlast   => m_ft_rx_tlast
	);

	FT_tx_fifo_inst : entity work.FT_data_FIFO
	port map(
		s_axis_aresetn => not rst,
		s_axis_aclk    => clk,
		m_axis_aclk    => clk_ftdi,
		s_axis_tvalid  => s_ft_tx_tvalid,
		s_axis_tready  => s_ft_tx_tready,
		s_axis_tdata   => s_ft_tx_tdata,
		s_axis_tlast   => s_ft_tx_tlast,
		m_axis_tvalid  => m_ft_tx_tvalid,
		m_axis_tready  => m_ft_tx_tready,
		m_axis_tdata   => m_ft_tx_tdata,
		m_axis_tlast   => open
	);

	FT_USB_FSM: entity work.FTDI_mFIFO
		port map(
			CLK   => clk_ftdi,
			DATA  => FT_DATA,
			RXF_N => FT_RXF_N,
			TXE_N => FT_TXE_N,
			OE_N  => FT_OE_N,
			RD_N  => FT_RD_N,
			WR_N  => FT_WR_N,
			rst       => rst,
			data_in   => m_ft_tx_tdata,
			data_out  => s_ft_rx_tdata,
			out_DV    => s_ft_rx_tvalid,
			in_DV     => m_ft_tx_tvalid,
			din_rd    => m_ft_tx_tready,
			pause_rcv => not s_ft_rx_tready
		);

	pkgr_conv_inst : entity work.pkgr_dwidth_converter
		port map (
			aclk => clk,
			aresetn => not rst,
			s_axis_tvalid => pkgr_valid,
			s_axis_tready => pkgr_ready,
			s_axis_tdata  => pkgr_data,
			s_axis_tlast  => pkgr_last,
			m_axis_tvalid => pkgr8_valid,
			m_axis_tready => pkgr8_ready,
			m_axis_tdata  => pkgr8_data,
			m_axis_tlast  => pkgr8_last
		);


	rxflag_gen: process(clk_ftdi)
	begin
		if rising_edge(clk_ftdi) then

			s_ft_rx_tlast <= '0';

			if s_ft_rx_tvalid = '1' and s_ft_rx_tready = '1' then

				rxcount <= rxcount + 1;

				if rxcount = 8 then
					s_ft_rx_tlast <= '1';
				end if;

				if rxcount = 9 then
					rxcount <= 0;
				end if;

			end if;

		end if;
	end process;


	ftmux: process(all)
	begin

		if daq_run = '1' then

			s_ft_tx_tvalid <= pkgr8_valid;
			s_ft_tx_tlast  <= pkgr8_last;
			s_ft_tx_tdata  <= pkgr8_data;

			pkgr8_ready <= s_ft_tx_tready;
			spw_txrdy  <= '0';

		else

			s_ft_tx_tvalid <= spw_txwrite;
			s_ft_tx_tlast  <= spw_txflag;
			s_ft_tx_tdata  <= spw_txdata;

			pkgr8_ready <= '0';
			spw_txrdy  <= s_ft_tx_tready;


		end if;

	end process;


	addrmux: process(all)
	begin

		regfile_we <= '0';
		regfile_data_in <= (others => '0');
		regfile_addr <= (others => '0');

		mcu_bram_we <= (others => '0');
		mcu_bram_di <= (others => '0');
		mcu_bram_addr <= (others => '0');

		-- The cpu bram use byte-addressing, the others word-addressing
		if (s_addr < x"0000_1000") then

			regfile_we <= s_we;
			regfile_data_in <= s_data_in;
			regfile_addr <= s_addr;
			s_data_out <= regfile_data_out;
			s_write_done <= regfile_write_done;

		else

			mcu_bram_we <= (others => s_we);
			mcu_bram_di <= s_data_in;
			mcu_bram_addr <= 19x"00" & s_addr(10 downto 0) & "00";
			s_data_out <= mcu_bram_do;
			s_write_done <= s_we;

		end if;


	end process addrmux;


	cmd_intr_gen: process(clk)
	begin

		if rising_edge(clk) then

			cmd_intr <= '0';

			if s_we = '1' and s_addr = cmd_addr then
				cmd_intr <= '1';
			end if;

		end if;

	end process;


	ft_bufg : BUFG
		port map(
			O => clk_ftdi,
			I => FT_CLK
		);

end wrap_bhv;
