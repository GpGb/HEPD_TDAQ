library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VComponents.all;

use work.types_pkg.all;

entity output_mux_TB is
	generic(
		n_staves : natural := 15
	);
end output_mux_TB;

architecture Behavioral of output_mux_TB is

	-- control signals
	signal clk, rst : std_logic := '0';
	signal tclk : time := 10 ns;
	signal trig_mask_ev : std_logic := '0';
	signal stave_mask : std_logic_vector(n_staves-1 downto 0) := (others => '0');
	signal trig_mask : std_logic_vector(4 downto 0) := (others => '0');
	signal out_mux_en, dec_skip_data : std_logic := '0';

	-- stave buffers
	signal stave_data_out : slv8_arr(0 to n_staves-1);
	signal stave_out_valid, stave_out_last, stave_out_ready :
		std_logic_vector(n_staves-1 downto 0) := (others => '0');

	-- output data
	signal data_out : std_logic_vector(31 downto 0);
	signal dout_DV, dout_ack, dout_last : std_logic;

	-- MCU pix fifo
	signal axis_pix_tdata : slv32;
	signal axis_pix_tlast, axis_pix_tready, axis_pix_tvalid : std_logic;

begin

	uut: entity work.output_mux
		generic map(n_staves => n_staves)
		port map(
			clk => clk,
			rst => rst,
			trig_mask_ev => trig_mask_ev,
			trig_mask  => trig_mask,
			trig_count => (others => '0'),
			timestamp  => (others => '0'),
			stave_mask => stave_mask,
			out_mux_en => out_mux_en,
			dec_skip_data => dec_skip_data,
			-- stave buffers
			data_in       => stave_data_out,
			data_in_valid => stave_out_valid,
			data_in_last  => stave_out_last,
			din_rd        => stave_out_ready,
			-- output data
			data_out  => data_out,
			dout_DV   => dout_DV,
			dout_last => dout_last,
			dout_ack  => dout_ack,
			-- MCU DPRAM
			axis_pix_tdata  => axis_pix_tdata,
			axis_pix_tlast  => axis_pix_tlast,
			axis_pix_tready => axis_pix_tready,
			axis_pix_tvalid => axis_pix_tvalid
		);


		clk <= not clk after tclk/2;

		stim: process

			procedure stave_send_data(stave_no : natural) is
			begin
				for i in 0 to 20 loop
					stave_out_valid(stave_no) <= '1';
					stave_data_out(stave_no) <= uint2slv(i, 8);
					stave_out_last(stave_no) <= '0';
					if i = 20 then
						stave_out_last(stave_no) <= '1';
					end if;

					if stave_out_ready(stave_no) = '0' then
						wait until stave_out_ready(stave_no) = '1';
					end if;
					wait for tclk;
				end loop;

				stave_out_valid <= (others => '0');
			end stave_send_data;

			procedure send_trigger(mask: std_logic_vector(4 downto 0)) is
			begin
				wait for Tclk;
				trig_mask_ev <= '1';
				trig_mask <= mask;
				wait for Tclk;
				trig_mask_ev <= '0';
				trig_mask <= (others => '0');
			end procedure send_trigger;

		begin

			rst <= '0';
			wait for Tclk * 20;
			rst <= '1';
			wait for Tclk * 100;
			rst <= '0';
			wait for Tclk * 20;
			wait until rising_edge(clk);

			-- trigger all staves
			send_trigger("11111");
			for n in 0 to n_staves-1 loop
				stave_send_data(n);
			end loop;

			wait for tclk*100;

			-- two turrent
			send_trigger("00110");
			for n in 3 to 8 loop
				stave_send_data(n);
			end loop;

			wait for tclk*100;

			-- zero turrets
			send_trigger("00000");
			wait for tclk*100;

			-- test stavemask
			for n in 0 to n_staves-1 loop
				stave_mask <= (others => '1');
				stave_mask(n) <= '0';
				wait for tclk*10;
				stave_send_data(n);
				wait for tclk*50;
			end loop;

			wait;
		end process;


		rcv: process
		begin
			dout_ack <= '1';
			wait;
		end process;

end architecture;
