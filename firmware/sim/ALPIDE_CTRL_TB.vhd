library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use work.ALPIDE_pkg.all;

entity ALPIDE_CTRL_TB is
end ALPIDE_CTRL_TB;

architecture Behavioral of ALPIDE_CTRL_TB is

	constant T_CLK : time := 25 us;
	signal clk : std_logic := '0';
	signal CTRL_in, CTRL_out, DIR : std_logic;

	signal data_out  : std_logic_vector(15 downto 0) := (others => '0');
	signal data_in   : std_logic_vector(47 downto 0) := (others => '0');
	signal RX_err : std_logic_vector(1 downto 0);
	signal test_data : std_logic_vector(34 downto 0);

	alias OP_code  : std_logic_vector(7 downto 0)  is data_in(47 downto 40);
	alias chip_id  : std_logic_vector(7 downto 0)  is data_in(39 downto 32);
	alias reg_addr : std_logic_vector(15 downto 0) is data_in(31 downto 16);
	alias data     : std_logic_vector(15 downto 0) is data_in(15 downto 0);

	signal start, done : std_logic := '0';

begin

	uut: entity work.ALPIDE_CTRL
		port map(
			clk => clk,
			CTRL_in  => CTRL_in,
			CTRL_out => CTRL_out,
			DIR  => DIR,
			-- internal interface
			data_in  => data_in,
			data_out => data_out,
			RX_err => RX_err,
			start    => start,
			done     => done
		);


		clk <= not clk after T_CLK / 2;

		stim: process is
		begin

			start <= '0';
			CTRL_in <= '1';
			wait for T_CLK * 10;
			wait until clk = '1';

			-- write operation
			OP_code <= ALPIDE_WROP;
			chip_id  <= x"01";
			reg_addr <= x"0000";
			data     <= x"0000";
			start <= '1';
			wait for T_CLK;
			start <= '0';

			wait until done = '1';
			wait for T_CLK * 20;

			-- cmd broadcast
			OP_code <= ALPIDE_TRIG;
			start <= '1';
			wait for T_CLK;
			start <= '0';

			wait until done = '1';
			wait for T_CLK * 20;

			-- read operation
			test_data <= "1111" & x"de" & "0111" & x"ad" & "01" & x"01" & '0';
			OP_code <= ALPIDE_RDOP;
			start <= '1';
			wait for T_CLK;
			start <= '0';
			-- wait for wr + turnaround
			wait for T_CLK * (40 + 15);
			-- shift out test data
			for n in 0 to 34 loop
				CTRL_in <= test_data(n);
				wait for T_CLK;
			end loop;

			wait until done = '1';

			wait;
		end process;

end Behavioral;
