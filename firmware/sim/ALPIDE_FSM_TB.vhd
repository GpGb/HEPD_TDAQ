library ieee;
use ieee.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
use work.ALPIDE_pkg.all;
use work.types_pkg.all;

entity ALPIDE_FSM_TB is
end ALPIDE_FSM_TB;

architecture Behavioral of ALPIDE_FSM_TB is

	constant Tclk   : time := 25 ns;
	signal clk, rst : std_logic := '0';

	-- ALPIDE FSM interface
	signal alp_cmd : std_logic_vector(47 downto 0);
	signal cmd_res  : std_logic_vector(15 downto 0);
	signal data_out : std_logic_vector(23 downto 0);
	signal data_out_strb : std_logic_vector(2 downto 0);
	signal cmd_DV, cmd_ack, cmd_err, res_DV : std_logic;
	signal data_valid, data_last, running : std_logic;
	signal buff_full, trig : std_logic := '0';

	-- CTRL interface
	signal ctrl_data_in  : std_logic_vector(47 downto 0);
	signal ctrl_data_out : std_logic_vector(15 downto 0);
	signal ctrl_rx_err   : std_logic_vector(1 downto 0);
	signal ctrl_done, ctrl_start : std_logic := '0';

	-- TB singals
	signal DMU_hi : std_logic := '0';
	constant chip_id_arr : slv8_arr :=(
		x"70", x"71", x"72", x"73", x"74",
		x"78", x"79", x"7a", x"7b", x"7c");
	signal test_chipmask : std_logic_vector(9 downto 0) := (others => '0');
	signal rcv_chip_id : std_logic_vector(7 downto 0);
	signal trig_rcvd, triggered : std_logic := '0';
	signal missing_chip_err : std_logic := '0';
	signal last_chip : std_logic := '0';
	-- data gen
	signal test_data : std_logic_vector(31 downto 0) := x"00ffffff";
	signal test_data_strb : std_logic_vector(2 downto 0) := "100";
	signal next_word_rq, inject_word : std_logic := '0';
	signal counter : natural := 0;
	-- data checker
	signal chk_data : std_logic_vector(23 downto 0);
	signal chk_strb : std_logic_vector(2 downto 0);
	signal first_run : std_logic := '0';

begin

	uut: entity work.ALPIDE_FSM
		port map(
			clk => clk,
			rst => rst,
			running => running,
			-- Command interface
			trig    => trig,
			cmd_in  => alp_cmd,
			cmd_DV  => cmd_DV,
			cmd_ack => cmd_ack,
			res_DV  => res_DV,
			cmd_err => cmd_err,
			-- Readout data output
			data_out      => data_out,
			data_out_strb => data_out_strb,
			data_last     => data_last,
			data_valid    => data_valid,
			buff_full    	=> buff_full,
			-- CTRL module interface
			CTRL_start    => ctrl_start,
			CTRL_done     => ctrl_done,
			CTRL_data_in  => ctrl_data_in,
			CTRL_data_out => ctrl_data_out,
			RX_err        => ctrl_rx_err
		);


	clk <= not clk after Tclk/2;

	stim: process

		procedure send_command(
			OP_code : in std_logic_vector(7 downto 0);
			chip_id : in std_logic_vector(7 downto 0);
			addr    : in std_logic_vector(15 downto 0);
			value   : in std_logic_vector(15 downto 0)
		) is
		begin
			alp_cmd <= (others => '0');
			alp_cmd(OP_code_rng) <= OP_code;
			alp_cmd(chip_id_rng) <= chip_id;
			alp_cmd(reg_addr_rng) <= addr;
			alp_cmd(data_rng) <= value;
			cmd_DV <= '1';
			wait until cmd_ACK = '1' for tclk*4;
			assert cmd_ack = '1' report "no command ackn received";
			wait for Tclk;
			cmd_DV <= '0';
		end procedure;

	begin
		-- reset
		alp_cmd <= (others => '0');
		cmd_DV <= '0';
		rst <= '1';
		wait for Tclk*50;
		wait until rising_edge(clk);
		rst <= '0';

		-- send RDOP command
		send_command(ALPIDE_RDOP, x"70", x"0001", x"0000");
		-- validate response
		wait until ctrl_done = '1' for tclk*40;
		assert ctrl_done = '1' report "rx timeout";
		wait until res_DV = '1' for tclk*2;
		assert res_DV = '1' report "expected command res_DV";
		assert ctrl_data_out = x"dead" report "unexpected reg value";
		assert ctrl_rx_err = "00" report "unexpected rx error";
		wait for tclk*4;

		-- readout test
		for n in 0 to 1 loop
			wait for tclk*10;
			wait until clk = '1';
			wait for tclk;
			trig <= '1';
			wait for tclk;
			trig <= '0';
			wait until running = '0';
			wait for Tclk*100;
		end loop;

		-- pulse command
		send_command(ALPIDE_PULSE, x"00", x"0000", x"0000");
		wait until running = '0';
		wait for tclk*10;

		--readout with dead chip
		missing_chip_err <= '1';
		wait for tclk;
		trig <= '1';
		wait for tclk;
		trig <= '0';
		wait until running = '0';
		wait for Tclk*100;
		missing_chip_err <= '0';


		-- write chipmask
		test_chipmask <= "1000010000";
		wait for tclk;
		send_command(FSM_SET_MSK, x"00", x"0000", "000000"&test_chipmask);


		wait;
	end process;


	-- check validity of commands to the ctrl interface and generate a simple response
	-- to read operations. During the redout procedure this also validates the correct
	-- sequence of commands.
	ctrl_sim: process
	begin

		wait until rising_edge(clk);

		-- wait start
		if rst = '1' then
			wait until rst = '0' and clk = '1';
		end if;
		if ctrl_start = '0' then
			wait until ctrl_start = '1';
		end if;

		wait for 10 * tclk; -- ctrl simulate command send time

		ctrl_rx_err <= "00";
		rcv_chip_id <= ctrl_data_in(chip_id_rng);

		if ctrl_data_in(OP_code_rng) = ALPIDE_TRIG or ctrl_data_in(OP_code_rng) = ALPIDE_PULSE then
			trig_rcvd <= '1';
		elsif missing_chip_err = '1' and ctrl_data_in(chip_id_rng) = x"71" then
			ctrl_data_out <= (others => '0');
			ctrl_rx_err <= "11";
		elsif ctrl_data_in(OP_code_rng) = ALPIDE_RDOP and (
			ctrl_data_in(reg_addr_rng) = ALPIDE_DMU_FIFO_LO or
			ctrl_data_in(reg_addr_rng) = ALPIDE_DMU_FIFO_HI) then
			-- handle rcv command
			wait for 20 * tclk;  -- simulate chip data reread time

			assert triggered = '1' report "rdout started without trigger";

			if DMU_hi = '0' then
				assert ctrl_data_in(reg_addr_rng) = ALPIDE_DMU_FIFO_LO
					report "unexpected register address";
				ctrl_data_out <= test_data(15 downto 0);
				DMU_hi <= '1';
			else
				assert ctrl_data_in(reg_addr_rng) = ALPIDE_DMU_FIFO_HI
					report "unexpected register address";
				ctrl_data_out <= test_data(31 downto 16);
				DMU_hi <= '0';
				next_word_rq <= '1';
			end if;
		elsif ctrl_data_in(OP_code_rng) = ALPIDE_RDOP then
			ctrl_data_out <= x"dead";
		else
			report "recv command:" & to_hstring(ctrl_data_in(OP_code_rng));
		end if;

		-- gnenrate done signal
		CTRL_done <= '1';
		wait for tclk;
		CTRL_done <= '0';
		next_word_rq <= '0';
		trig_rcvd <= '0';

	end process;


	-- generate readout data, including the current chip id, that will be
	-- used by the ctrl_sim process.
	data_generator: process
		variable hitmap : std_logic_vector(6 downto 0);
		constant data_words_max : natural := 4;
	begin
		wait until rising_edge(clk);

		if trig_rcvd = '1' then
			triggered <= '1';
		end if;

		if next_word_rq = '1' then

			if inject_word = '1' then
				test_data <= x"00ff_ffff";
			elsif counter = 0 then
				-- chip header
				test_data <= x"00" & "1010" & rcv_chip_id(3 downto 0) & x"00ff";
				test_data_strb <= "110";
			elsif counter = 1 then
				-- region header
				test_data <= x"00" & "110" & "00001" & x"ffff";
				test_data_strb <= "100";
			elsif counter < data_words_max then
				-- data long with zero enc_id + addr and counter as hitmap
				hitmap := uint2slv(counter, 7);
				test_data_strb <= "111";
				test_data <= x"00" & "00" & 14x"0" & "0" & hitmap;
			else
				-- chip trailer
				test_data_strb <= "100";
				test_data <= x"00" & "1011" & x"0" & x"ffff";
			end if;

			if inject_word = '0' then
				if counter = data_words_max then
					counter <= 0;
				else
					counter <= counter + 1;
				end if;
			end if;

			if counter = data_words_max then
				wait until ctrl_done = '1';
				wait for tclk;
				wait until ctrl_done = '1';
				if last_chip = '1' then
					triggered <= '0';
				end if;
			end if;

		end if;

	end process;


	chipid_mgr: process
		variable last_chip_id : std_logic_vector(7 downto 0);
	begin
		wait until rising_edge(clk);

		for n in chip_id_arr'length-1 downto 0 loop
			if test_chipmask(n) = '0' then
				last_chip_id := chip_id_arr(n);
				exit;
			end if;
		end loop;

		last_chip <= '0';
		wait until triggered = '1';
		wait until rcv_chip_id = last_chip_id;
		last_chip <= '1';
		wait until triggered = '0';
	end process;


	-- validate data output
	data_checker: process
	begin
		wait until rising_edge(clk);

		if triggered = '1' then

			-- by TB design fist non-injected word is idle, so skip first word
			if first_run = '0' then
				first_run <= '1';
				wait until next_word_rq = '1';
				wait for tclk;
			end if;

			wait until next_word_rq = '1' and clk = '1';
			chk_data <= test_data(23 downto 0);
			chk_strb <= test_data_strb;
			if inject_word = '1' then
				wait for tclk;
				assert data_valid = '0' report "unexpected data on output";
			else
				wait until data_valid = '1' for Tclk*30;
				assert data_valid = '1' report "missing data";
				assert data_out = chk_data report "rdout data mismatch," &
					" rcv: " & to_hstring(data_out) &
					" exp: " & to_hstring(chk_data);
				assert data_out_strb = chk_strb report "wrong strobe";
				wait for tclk;
			end if;

		else
			wait for tclk;
		end if;

	end process;


	-- check generation of error trailer
	chk_error_trailer: process
	begin
		wait until rising_edge(clk);

		wait until missing_chip_err = '1' and rcv_chip_id = x"71";
		wait until data_valid = '1';
		assert data_out = EV_CHIP_SKIPPED report "missing/wrong error trailer";
		assert data_out_strb = "100" report "wrong error trailer strobe";

		if missing_chip_err = '1' then
			wait until missing_chip_err = '0';
		end if;

	end process;

	-- check generation of event trailer
	chk_event_trailer: process
	begin
		wait until rising_edge(clk);

		wait until triggered = '1';
		wait for Tclk;
		wait until triggered = '0';

		wait until data_last = '1';

		assert data_last = '1' report "missing data_last flag" severity failure;
		assert data_out(23 downto 20) = x"e" report "missing/wrong event trailer";
		assert data_out_strb = "111" report "wrong event trailer strobe";

	end process;

end Behavioral;
