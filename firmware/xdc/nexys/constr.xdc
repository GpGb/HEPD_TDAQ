# FT FIFO clock
create_clock -name FT_CLK -period 16.667 [get_ports FT_CLK]

# forwarded alpide clocks
create_generated_clock -name ALP_clk_0  -source [get_pins gen_staves[0].CTRL_mng/O_ALP_clk/C]  -divide_by 1 -invert [get_ports ALP_clk[0]]
create_generated_clock -name ALP_clk_1  -source [get_pins gen_staves[1].CTRL_mng/O_ALP_clk/C]  -divide_by 1 -invert [get_ports ALP_clk[1]]
create_generated_clock -name ALP_clk_2  -source [get_pins gen_staves[2].CTRL_mng/O_ALP_clk/C]  -divide_by 1 -invert [get_ports ALP_clk[2]]
#create_generated_clock -name ALP_clk_3  -source [get_pins gen_staves[3].CTRL_mng/O_ALP_clk/C]  -divide_by 1 -invert [get_ports ALP_clk[3]]
#create_generated_clock -name ALP_clk_4  -source [get_pins gen_staves[4].CTRL_mng/O_ALP_clk/C]  -divide_by 1 -invert [get_ports ALP_clk[4]]
#create_generated_clock -name ALP_clk_5  -source [get_pins gen_staves[5].CTRL_mng/O_ALP_clk/C]  -divide_by 1 -invert [get_ports ALP_clk[5]]
#create_generated_clock -name ALP_clk_6  -source [get_pins gen_staves[6].CTRL_mng/O_ALP_clk/C]  -divide_by 1 -invert [get_ports ALP_clk[6]]
#create_generated_clock -name ALP_clk_7  -source [get_pins gen_staves[7].CTRL_mng/O_ALP_clk/C]  -divide_by 1 -invert [get_ports ALP_clk[7]]
#create_generated_clock -name ALP_clk_8  -source [get_pins gen_staves[8].CTRL_mng/O_ALP_clk/C]  -divide_by 1 -invert [get_ports ALP_clk[8]]
#create_generated_clock -name ALP_clk_9  -source [get_pins gen_staves[9].CTRL_mng/O_ALP_clk/C]  -divide_by 1 -invert [get_ports ALP_clk[9]]
#create_generated_clock -name ALP_clk_10 -source [get_pins gen_staves[10].CTRL_mng/O_ALP_clk/C] -divide_by 1 -invert [get_ports ALP_clk[10]]
#create_generated_clock -name ALP_clk_11 -source [get_pins gen_staves[11].CTRL_mng/O_ALP_clk/C] -divide_by 1 -invert [get_ports ALP_clk[11]]
#create_generated_clock -name ALP_clk_12 -source [get_pins gen_staves[12].CTRL_mng/O_ALP_clk/C] -divide_by 1 -invert [get_ports ALP_clk[12]]
#create_generated_clock -name ALP_clk_13 -source [get_pins gen_staves[13].CTRL_mng/O_ALP_clk/C] -divide_by 1 -invert [get_ports ALP_clk[13]]
#create_generated_clock -name ALP_clk_14 -source [get_pins gen_staves[14].CTRL_mng/O_ALP_clk/C] -divide_by 1 -invert [get_ports ALP_clk[14]]

# CTRL
set_property IOB true [get_ports ALP_ctrl_R[*]]
set_property IOB true [get_ports ALP_ctrl_D[*]]
