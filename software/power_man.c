#include "platform.h"
#if PLATFORM_TYPE == __NEXYS__

#include "xparameters.h"
#include <sleep.h>
#include <math.h>
#include "alpide.h"
#include "xiic.h"
#include <stdio.h>

#define IIC_BASE_ADDRESS XPAR_IIC_0_BASEADDR
#define IIC_MUX_ADDRESS 0x4c
#define MUX_REG_DIR0 0
#define MUX_REG_DIR1 1
#define MUX_REG_DIR2 2
#define MUX_REG_DIR3 3


int iic_mux_wr_reg(uint8_t reg_addr, uint8_t reg_data){

	volatile unsigned AckByteCount;
	uint8_t WriteBuffer[2];
	int Index;
	uint32_t CntlReg;

	uint8_t iic_mux_addr = IIC_MUX_ADDRESS;
	WriteBuffer[0] = reg_addr;
	WriteBuffer[1] = reg_data;

	/*
	* Set the address register to the specified address by writing
	* the address to the device, this must be tried until it succeeds
	* because a previous write to the device could be pending and it
	* will not ack until that write is complete.
	*/
	volatile unsigned SentByteCount;
	do {
		SentByteCount = XIic_Send(IIC_BASE_ADDRESS,
				IIC_MUX_ADDRESS,
				&iic_mux_addr, sizeof(iic_mux_addr),
				XIIC_STOP);
		if (SentByteCount != sizeof(iic_mux_addr)) {

			/* Send is aborted so reset Tx FIFO */
			CntlReg = XIic_ReadReg(IIC_BASE_ADDRESS,
					XIIC_CR_REG_OFFSET);
			XIic_WriteReg(IIC_BASE_ADDRESS, XIIC_CR_REG_OFFSET,
					CntlReg | XIIC_CR_TX_FIFO_RESET_MASK);
			XIic_WriteReg(IIC_BASE_ADDRESS, XIIC_CR_REG_OFFSET,
					XIIC_CR_ENABLE_DEVICE_MASK);
		}

	} while (SentByteCount != sizeof(iic_mux_addr));

	/*
	* Write data
	*/
	SentByteCount = XIic_Send(IIC_BASE_ADDRESS, IIC_MUX_ADDRESS,
			WriteBuffer, 2,
			XIIC_STOP);

	/*
	* Wait for the write to be complete by trying to do a write and
	* the device will not ack if the write is still active.
	*/
	do {
		AckByteCount = XIic_Send(IIC_BASE_ADDRESS, IIC_MUX_ADDRESS,
				&iic_mux_addr, sizeof(iic_mux_addr),
				XIIC_STOP);
		if (AckByteCount != sizeof(iic_mux_addr)) {

			/* Send is aborted so reset Tx FIFO */
			CntlReg = XIic_ReadReg(IIC_BASE_ADDRESS,
					XIIC_CR_REG_OFFSET);
			XIic_WriteReg(IIC_BASE_ADDRESS, XIIC_CR_REG_OFFSET,
					CntlReg | XIIC_CR_TX_FIFO_RESET_MASK);
			XIic_WriteReg(IIC_BASE_ADDRESS, XIIC_CR_REG_OFFSET,
					XIIC_CR_ENABLE_DEVICE_MASK);
		}

	} while (AckByteCount != sizeof(iic_mux_addr));


	if (SentByteCount != 2)
		return -1;

	return 0;
}


int iic_mux_rd_reg(uint8_t address, uint8_t *reg_data){

	uint8_t iic_mux_addr = IIC_MUX_ADDRESS;
	uint32_t CntlReg;

	/*
	* Set the address register to the specified address by writing
	* the address to the device, this must be tried until it succeeds
	* because a previous write to the device could be pending and it
	* will not ack until that write is complete.
	*/
	volatile unsigned ReceivedByteCount;
	do {
		uint16_t StatusReg = XIic_ReadReg(IIC_BASE_ADDRESS, XIIC_SR_REG_OFFSET);
		if(!(StatusReg & XIIC_SR_BUS_BUSY_MASK)) {
			ReceivedByteCount = XIic_Send(IIC_BASE_ADDRESS,
							IIC_MUX_ADDRESS,
							&iic_mux_addr,
							sizeof(iic_mux_addr),
							XIIC_STOP);

			if (ReceivedByteCount != sizeof(iic_mux_addr)) {

				/* Send is aborted so reset Tx FIFO */
				CntlReg = XIic_ReadReg(IIC_BASE_ADDRESS,
							XIIC_CR_REG_OFFSET);
				XIic_WriteReg(IIC_BASE_ADDRESS, XIIC_CR_REG_OFFSET,
						CntlReg | XIIC_CR_TX_FIFO_RESET_MASK);
				XIic_WriteReg(IIC_BASE_ADDRESS,
						XIIC_CR_REG_OFFSET,
						XIIC_CR_ENABLE_DEVICE_MASK);
			}
		}

	} while (ReceivedByteCount != sizeof(iic_mux_addr));

	// Read the number of bytes at the specified address from the EEPROM.
	ReceivedByteCount = XIic_Recv(IIC_BASE_ADDRESS, IIC_MUX_ADDRESS,
					reg_data, 1, XIIC_STOP);

	if (ReceivedByteCount != 1)
		return -1;

	return 0;
}

static uint16_t last_addr = 0;

// a=gnd b=vcc
int pm_tsp_switch_set(uint8_t staveid, uint8_t channel, uint8_t setrst){

	dbg_print("staveid: %x, channel: %x, setrst: %d\n", staveid, channel, setrst);
	uint16_t address_b = last_addr;
	uint16_t address_a = ~address_b;

	uint16_t index = staveid + channel*3;
	if (index > 7)
		index = 15;

	int ret;
	if (setrst == 1){
		address_b |= (1 << index);
		address_a = ~address_b;

		ret  = iic_mux_wr_reg(MUX_REG_DIR0, address_a&0xff);
		ret |= iic_mux_wr_reg(MUX_REG_DIR1, address_a>>8);
		if (ret < 0)
			return ret;

		ret  = iic_mux_wr_reg(MUX_REG_DIR2, address_b&0xff);
		ret |= iic_mux_wr_reg(MUX_REG_DIR3, address_b>>8);
	}
	else{
		address_b &= ~(1 << index);
		address_a = ~address_b;

		ret  = iic_mux_wr_reg(MUX_REG_DIR2, address_b&0xff);
		ret |= iic_mux_wr_reg(MUX_REG_DIR3, address_b>>8);
		if (ret < 0)
			return ret;

		ret  = iic_mux_wr_reg(MUX_REG_DIR0, address_a&0xff);
		ret |= iic_mux_wr_reg(MUX_REG_DIR1, address_a>>8);
	}

	last_addr = address_b;

	dbg_print("reg: %x\n", address_b);
	return ret;
}

int pm_dump_iicmux_regs(){

	uint8_t reg_data;
	int ret;

	for(uint8_t i = 0; i < 4; i++){
		ret = iic_mux_rd_reg(i, &reg_data);
		dbg_print("rd_mux_reg: %d, %x\n", ret, reg_data);
	}
	return 0;
}

int pm_tsp_poweroff(){

	uint16_t address_b = 0;
	uint16_t address_a = 0xffff;

	int ret;
	ret  = iic_mux_wr_reg(MUX_REG_DIR2, address_b&0xff);
	ret |= iic_mux_wr_reg(MUX_REG_DIR3, address_b>>8);
	if (ret < 0)
		return ret;

	ret  = iic_mux_wr_reg(MUX_REG_DIR0, address_a&0xff);
	ret |= iic_mux_wr_reg(MUX_REG_DIR1, address_a>>8);

	last_addr = 0;
	return ret;
}

int pm_tsp_poweron(){

	int ret = 0;

	for(int stave_id=0; stave_id<3; stave_id++){

		ret = pm_tsp_switch_set(stave_id, 0, 1);
		if (ret < 0){
			pm_tsp_poweroff();
			break;
		}

		ret = pm_tsp_switch_set(stave_id, 1, 1);
		if (ret < 0){
			pm_tsp_poweroff();
			break;
		}

		usleep(1000);

		ret = pm_tsp_switch_set(stave_id, 2, 1);
		if (ret < 0){
			pm_tsp_poweroff();
			break;
		}

		usleep(10000);
	}

	return ret;
}


int pm_init_power_system(){

	int ret = pm_tsp_switch_set(0, 0, 0);
	return ret;

}

#endif
