#ifndef SRC_flash_H_
#define SRC_flash_H_

#define PAGE_SIZE       256

int init_flash_subsystem();
int SpiFlashRead(uint32_t Addr, uint32_t ByteCount, uint8_t* Buffer);
int SpiFlashWrite(uint32_t Addr, uint32_t ByteCount, uint8_t* Buffer);
int SpiFlashBulkErase();
int SpiFlashSectorErase(uint32_t Addr, uint32_t size);

#endif
