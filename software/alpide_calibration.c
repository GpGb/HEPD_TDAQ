#include "platform.h"
#include "alpide.h"
#include "spw_dpram.h"
#include "FIFO.h"
#include "ffs_io.h"
#include "intc.h"

#define ITHR_MIN 50           // min ITHR value for thresh calibration
#define ITHR_MAX 80           // max ITHR value for thresh calibration
#define VCASN_MAX_STEPS 10    // max steps for VCANS for thresh calibration
#define VCASN2_ALICE_MAGIC 12 // VCANS2 = VCANS + VCASN2_ALICE_MAGIC
#define THRSCAN_STEPS 30      // charge steps for thr scan
#define THRSCAN_INJS  20      // number of injections per charge step


uint16_t threshold_res[150] = {0};


// --------------------------------------------------------------------------
// ------------------------ DIGI and THRESH scans ---------------------------
// --------------------------------------------------------------------------

int update_hist_counter(uint16_t chip_id, uint32_t *counter){

	uint8_t id_lsb = chip_id&0xf;

	if (read_pix_fifo() < 0)
		return -1;

	if (RX_packet_buffer.count == 0)
		return 0;

	for(int index=0; index < RX_packet_buffer.count/4-1; index += 4){

		uint32_t cnt_data;
		memcpy(&cnt_data, &RX_packet_buffer.data[index], 4);

		uint8_t id = (cnt_data >> 11) & 0xf;
		if (id == id_lsb)
			*counter += cnt_data & 0x7ff;

	}

	return 0;
}



int digi_scan(uint16_t chip_id, uint8_t mode, uint32_t *hitcnt){

	const uint16_t stave_brdcst_addr = (chip_id&0xff00) | 0xf;

	// pulse one double column at time
	for(uint16_t col=0; col<512; col++){
		int pix_col_reg_address =  column_pixreg_addr(col*2);
		int pix_col_reg_value = 3<<((col*2)&0xf);
		if ((mode&0xf) == 0x3){
			// unmask a double col
			write_alp_reg(chip_id, ALPIDE_REG_PIXCFG, 0x0);
			write_alp_reg(chip_id, pix_col_reg_address, pix_col_reg_value);
			write_alp_reg(chip_id, 0x484, 0xffff); //all rows
			write_alp_reg(chip_id, 0x487, 0x0000);
		}
		// enable pulser in double col
		write_alp_reg(chip_id, ALPIDE_REG_PIXCFG, 0x3);
		write_alp_reg(chip_id, pix_col_reg_address, pix_col_reg_value);
		write_alp_reg(chip_id, 0x484, 0xffff);
		write_alp_reg(chip_id, 0x487, 0x0000);

		uint32_t counter = 0;

		// send pulses, the pulse must be sent to all chip in the selected stave(s)
		for(int i=0; i<10; ++i){
			send_alpide_cmd(ALPIDE_PULSE, stave_brdcst_addr, 0, 0, NULL);
			BRAM_COMMAND_PROGRESS(((i+1) | (col<<8)));

			wait_nBusy(chip_id);
			if ((mode>>7) == 0){
				update_hist_counter(chip_id, &counter);
			}
		}

		*hitcnt += counter;

		// reset mask/pulse
		set_reset_pixregs(chip_id, 'm', 1);
		set_reset_pixregs(chip_id, 'p', 0);
	}

	// white frame scan might need a pixel matrix reset (..why?)
	send_alpide_cmd(0xe4, stave_brdcst_addr, 0, 0, NULL);

	return 0;
}



int thr_scan_full(uint16_t chip_id, uint32_t hist[THRSCAN_STEPS], uint8_t mode){

	const uint16_t stave_brdcst_addr = (chip_id&0xff00) | 0xf;

	for(int n=0; n<THRSCAN_STEPS; ++n){
		// pulse one double column at each cycle
		for(int col=0; col<512; col++){
			int pix_col_reg_address =  column_pixreg_addr(col*2);
			int pix_col_reg_value = 3<<((col*2)&0xf);
			// unmask a double col
			write_alp_reg(chip_id, ALPIDE_REG_PIXCFG, 0x0);
			write_alp_reg(chip_id, pix_col_reg_address, pix_col_reg_value);
			write_alp_reg(chip_id, 0x484, 0xffff); //all rows
			write_alp_reg(chip_id, 0x487, 0x0000);
			// enable pulser in double col
			write_alp_reg(chip_id, ALPIDE_REG_PIXCFG, 0x3);
			write_alp_reg(chip_id, pix_col_reg_address, pix_col_reg_value);
			write_alp_reg(chip_id, 0x484, 0xffff);
			write_alp_reg(chip_id, 0x487, 0x0000);

			// send pulses, the pulse must be sent to all chip in the selected stave(s)
			for(int i=0; i<THRSCAN_INJS; ++i){
				send_alpide_cmd(ALPIDE_PULSE, stave_brdcst_addr, 0, 0, NULL);
				BRAM_COMMAND_PROGRESS(((i+1) | (col<<8) | (n<<24)));

				wait_nBusy(chip_id);
				if (mode == 0){
					update_hist_counter(chip_id, &hist[n]);
				}

				if (command_rcv_intr == 2){
					return 1;
				}

			}

			// reset mask/pulse
			set_reset_pixregs(chip_id, 'm', 1);
			set_reset_pixregs(chip_id, 'p', 0);

		}

		write_alp_reg(chip_id, ALPIDE_REG_VPULSEL, 0xa9-n);

	}

	return 0;

}


int thr_scan_grid(uint16_t chip_id, uint32_t hist[THRSCAN_STEPS], uint8_t mode){

	const uint16_t stave_brdcst_addr = (chip_id&0xff00) | 0xf;

	// unmask chipid
	set_reset_pixregs(chip_id, 'm', 0);

	// pulse a dcol at time
	for(int n=0; n<THRSCAN_STEPS; ++n){
		// pulse one double column at each cycle
		for(int col=32; col<512; col++){
			if (col%32 >= 2) continue;
			int pix_col_reg_address =  column_pixreg_addr(col*2);
			int pix_col_reg_value = 3<<((col*2)&0xf);
			write_alp_reg(chip_id, ALPIDE_REG_PIXCFG, 0x3);

			// select double col
			write_alp_reg(chip_id, pix_col_reg_address, pix_col_reg_value);

			// select rows
			for(int n=0; n<8; n++){
				int pix_col_reg_address = row_pixreg_addr(n*64+32);
				int pix_col_reg_value = 0xf;
				write_alp_reg(chip_id, pix_col_reg_address, pix_col_reg_value);
			}
			write_alp_reg(chip_id, 0x487, 0x0000);

			// send pulses, the pulse must be sent to all chip in the selected stave(s)
			for(int i=0; i<THRSCAN_INJS; ++i){
				send_alpide_cmd(ALPIDE_PULSE, stave_brdcst_addr, 0, 0, NULL);
				BRAM_COMMAND_PROGRESS(((i+1) | (col<<8) | (n<<24)));

				wait_nBusy(chip_id);
				if (mode == 0){
					update_hist_counter(chip_id, &hist[n]);
				}

				if (command_rcv_intr == 2){
					return 1;
				}

			}

			// reset pulse
			set_reset_pixregs(chip_id, 'p', 0);

		}

		write_alp_reg(chip_id, ALPIDE_REG_VPULSEL, 0xa9-n);

	}

	return 0;

}


uint16_t thr_from_hist(const uint32_t *hist){

	uint32_t sum = 0;
	double avg = .0, var = .0;

	for(int i=0; i<THRSCAN_STEPS-1; i++){
		uint32_t diff = (hist[i+1] > hist[i]) ? hist[i+1] - hist[i] : 0;
		double bin_center = i + 0.5;
		sum += diff;
		avg += diff * (bin_center);
		var += diff * (bin_center*bin_center);
	}

	avg /= sum;
	var = var/sum - avg*avg;

	uint16_t cal_result = (int)avg | (int)(var)<<8;


	return cal_result;

}


// mode[0:4]; 0: fast thrscan, 1: thrscan, 2: digiscan, 4: white framescan
// mode[8]; 0: local data process, 1: remote data process
int run_chip_pm_scan(uint16_t chip_id, uint8_t mode){

	const uint8_t chip_index = chipid_to_index(chip_id&0xff);
	const uint8_t stave_id = (chip_id>>8);
	const uint16_t stave_brdcst_addr = (stave_id&0xff00) | 0xf;
	const uint8_t mode_lsb = mode&0xf;
	const uint8_t mode_msb = (mode>>7);

	// no stave_id broadcast allowed
	if (stave_id == 0xf || (chip_id&0xf) == 0xf)
		return -1;

	// set readout stave mask
	uint32_t old_rdoutmux_reg;
	read_ctrl_reg(ALP_CMD_RDOUTMUX_REG_OFF, &old_rdoutmux_reg);

	uint32_t stave_mask = (mode_msb) ? 0 : (3<<16);
	stave_mask |= ~(1 << stave_id) & 0xffff;
	write_ctrl_reg(ALP_CMD_RDOUTMUX_REG_OFF, stave_mask);

	// setup pulser confs
	uint16_t fromu_cfg = (mode_lsb < 2) ? 0x70 : 0x50;
	write_alp_reg(chip_id, ALPIDE_REG_FROMU1, fromu_cfg);
	write_alp_reg(chip_id, ALPIDE_REG_VPULSEH, 0xaa);
	write_alp_reg(chip_id, ALPIDE_REG_VPULSEL, 0xa9);

	// mask all chips and disable pulser on all chip
	set_reset_pixregs(stave_brdcst_addr, 'm', 1);
	set_reset_pixregs(stave_brdcst_addr, 'p', 0);

	// exclude from readout all chips different from chipd_id
	uint16_t fsm_rdmask = ~(1<<chip_index);
	set_chipmask(stave_id, fsm_rdmask);

	// run thrscan
	if (mode_lsb < 2){

		uint32_t counter[THRSCAN_STEPS] = {0};

		if (mode_lsb == 0)
			thr_scan_grid(chip_id, counter, mode_msb);
		else
			thr_scan_full(chip_id, counter, mode_msb);

		// calculate threshold
		if (mode_msb == 0)
			threshold_res[chipid_to_index(chip_id)] = thr_from_hist(counter);
	}
	// run digiscan
	else {
		uint32_t hitcnt = 0;
		digi_scan(chip_id, mode, &hitcnt);
		if (mode_msb == 0){
			bram_write_scratch(&hitcnt, sizeof(hitcnt)); //TODO: avoid doing this here..
		}
	}

	// unmaks everything and reset mux mask
	set_reset_pixregs(stave_brdcst_addr, 'm', 0);
	write_ctrl_reg(ALP_CMD_RDOUTMUX_REG_OFF, old_rdoutmux_reg);
	set_chipmask(stave_id, 0);

	return 0;
}


int run_stave_pm_scan(uint8_t stave_id, uint8_t mode){

	if (stave_id >= 0xf)
		return -1;

	uint16_t chipmask = get_chipmask(stave_id);

	for(uint8_t id=0; id<10; id++){
		// skip masked chips
		if ( ((chipmask>>id)&0x1) == 1 ){
			continue;
		}

		uint16_t chip_id = index_to_chipid(id+stave_id*10);
		run_chip_pm_scan(chip_id, mode);

		if (command_rcv_intr == 2){
			return 1;
		}

	}

	if (mode<2){
		bram_write_scratch(threshold_res, sizeof(threshold_res));
	}

	bram_write_done();

	// restore chipmask
	set_chipmask(stave_id, chipmask);

	return 0;
}


// --------------------------------------------------------------------------
// ---------------------------- THR calibration -----------------------------
// --------------------------------------------------------------------------

int save_DAC_values(uint8_t stave_id, uint8_t findex){

	const uint16_t DAC_regs[] = {ALPIDE_REG_ITHR, ALPIDE_REG_VCASN, ALPIDE_REG_VCASN2};

	spiffs_file fd = ffs_open(index_to_fname(findex), SPIFFS_CREAT | SPIFFS_RDWR | SPIFFS_DIRECT, 0);
	if (fd < 0){
		dbg_print("fail to open file: %d\n\r", ffs_errno());
		return ffs_errno();
	}

	int retcode = 0, line_found = 0;

	for(int i=0; i<10; i++){
		uint16_t chip_id = index_to_chipid(stave_id*10 + i);

		for(int reg=0; reg<sizeof(DAC_regs)/sizeof(DAC_regs[0]); reg++){

			BRAM_COMMAND_PROGRESS(i | reg<<8);

			uint16_t reg_data = 0;
			int ret = read_alp_reg(chip_id, DAC_regs[reg], &reg_data);
			if (ret){
				retcode = -1;
				break;
			}

			ffs_lseek(fd, 0, SPIFFS_SEEK_SET);
			line_found = 0;
			uint16_t conf_data[3];

			while (!ffs_eof(fd)){
				int32_t len = ffs_read(fd, conf_data, sizeof(conf_data));
				if (len != sizeof(conf_data)){
					ffs_lseek(fd, -len, SPIFFS_SEEK_CUR);
					continue;
				}

				if (chip_id == conf_data[0] && DAC_regs[reg] == conf_data[1]){
					conf_data[2] = reg_data;
					ffs_lseek(fd, -sizeof(conf_data), SPIFFS_SEEK_CUR);
					ffs_write(fd, conf_data, sizeof(conf_data));
					line_found = 1;
					break;
				}

			}

			// add new line
			if (line_found == 0){
				conf_data[0] = chip_id;
				conf_data[1] = DAC_regs[reg];
				conf_data[2] = reg_data;
				ffs_write(fd, conf_data, sizeof(conf_data));
			}

		}

	}

	ffs_close(fd);
	return retcode;
}


uint16_t find_vcasn(uint16_t chip_id, uint16_t vcasn0, uint32_t target){

	write_alp_reg(chip_id, ALPIDE_REG_ITHR, ITHR_MIN);

	uint16_t vcasn = vcasn0;

	for(int n=0; n<VCASN_MAX_STEPS; n++){
		write_alp_reg(chip_id, ALPIDE_REG_VCASN, vcasn);
		write_alp_reg(chip_id, ALPIDE_REG_VCASN2, vcasn+VCASN2_ALICE_MAGIC);

		run_chip_pm_scan(chip_id, 0);
		dbg_print("%x) scan %d: curr_thr: %d, target: %d\n\t", chip_id, n,
				threshold_res[chipid_to_index(chip_id)]&0xff, target);

		if ((threshold_res[chipid_to_index(chip_id)]&0xff) < target)
			break;

		vcasn++;
	}

	return vcasn;
}


uint16_t find_ithr(uint16_t chip_id, uint32_t thr_target){

	uint8_t p0 = threshold_res[chipid_to_index(chip_id)];

	write_alp_reg(chip_id, ALPIDE_REG_ITHR, ITHR_MAX);
	run_chip_pm_scan(chip_id, 0);

	uint8_t p1 = threshold_res[chipid_to_index(chip_id)];

	//get optimal ithr
	uint16_t ithr = (ITHR_MAX-ITHR_MIN)/(p1-p0) * (thr_target-p0) + ITHR_MIN;

	write_alp_reg(chip_id, ALPIDE_REG_ITHR, ithr);
	return ithr;
}


int run_threshold_cal(uint8_t stave_id, uint16_t vcasn0, uint8_t target){

	dbg_print("starting scan stave_id %x, vcasn0 %x, target %d\n\t",
			stave_id, vcasn0, target);

	uint16_t chipmask = get_chipmask(stave_id);

	for(int i=0; i<10; i++){

		// skip masked chips
		if ( ((chipmask>>i)&0x1) == 1 ){
			continue;
		}

		uint16_t chip_id = index_to_chipid(stave_id*10 + i);

		// verify if current threshold is accettable
		run_chip_pm_scan(chip_id, 0);
		uint8_t curr_thr = threshold_res[chipid_to_index(chip_id)] & 0xff;
		if (abs(curr_thr-target) <= 1){
			dbg_print("%x) current thr %d already at target %d, skipping..",
					chip_id, curr_thr, target);
			continue;
		}

		// run calibration
		uint16_t vcasn = find_vcasn(chip_id, vcasn0, target);
		uint16_t ithr = find_ithr(chip_id, target);

		dbg_print("%x) done; vcans %x, ithr %x\n\t", chip_id, vcasn, ithr);
	}

	// restore chipmask
	set_chipmask(stave_id, chipmask);

	return 0;
}
