import serial
import random
import argparse

ser = serial.Serial('/dev/ttyUSB0', baudrate=115200)

no_error_cnt = 0
crc_only = 0
bad_err_cnt = 0
corrected_cnt = 0
uncorr_err_cnt = 0


def go_idle():
    ser.write(b'I')

    data = ser.read_until(b'I> ').decode().split('\r')
    assert data[1] == "SC 00"


def go_osbserve():
    ser.write(b'O')
    data = ser.read_until(b'O> ').decode().split('\r')

    assert data.pop(0) == 'O'
    assert data.pop(0) == 'SC 02'

    global no_error_cnt, crc_only, bad_err_cnt, corrected_cnt, uncorr_err_cnt

    if ser.in_waiting == 0:
        no_error_cnt += 1
        return

    data = ser.read_until(b'O> ').decode().split('\r')

    if 'SED OK' in data:
        corrected_cnt += data.count('SED OK')

    if 'SED NG' in data:
        bad_err_cnt += data.count('SED ND')

    if 'DED' in data:
        bad_err_cnt += data.count('DED')

    if 'CRC' in data:
        crc_only += data.count('CRC')

    if 'FC 60' in data:
        uncorr_err_cnt += data.count('FC 60')


def inject_error(command=None, min_addr = 0, max_addr=0):
    go_idle()

    if command == None:
        if max_addr == 0: max_addr = 32415744
        num  = (0b11 << 38)
        num |= random.randint(min_addr, max_addr)
        command = f'N {num:0{10}X}' # 10 digit hex, uppercase, no 0x prefix

    ser.write(command.encode())

    data = ser.read(28).decode().split('\r')
    assert data[0] == command
    assert data[1] == 'SC 10' and data[2] == 'SC 00'

    go_osbserve()

    return command



def inject_file(injfile):

    try:
        fin = open(injfile, 'r')
    except:
        print('Can\'t open file:', injfile)
        return

    for command in fin:
        inject_error(command[:-1])

        if bad_err_cnt > 0:
            break

    fin.close()



def inject_rand(min_addr, max_addr, iters, logfile_name = None):

    if logfile_name != None:
        logfile = open(logfile_name, 'w');

    for i in range(iters):
        address = inject_error(min_addr = min_addr, max_addr = max_addr)
        if args.logfile:
            logfile.write(address + '\n')

        if bad_err_cnt > 0:
            break

    if logfile_name != None:
        logfile.close()


if __name__ == '__main__':


    def str_to_int(x):
        return int(x, 0)

    parser = argparse.ArgumentParser()

    parser.add_argument('-n', type=str_to_int, default=1,
            help='number of iterations')

    parser.add_argument('--min', type=str_to_int, default=0,
            help='minimum address')
    parser.add_argument('--max', type=str_to_int, default=0,
            help='maximum address')

    parser.add_argument('--mode', type=str, default='random')
    parser.add_argument('-f', '--file', type=str)
    parser.add_argument('--logfile', type=str, default='injaddr.log')

    args = parser.parse_args()

    match args.mode:
        case 'list':
            inject_file(args.file)
        case 'random':
            inject_rand(args.min, args.max, args.n, logfile_name=args.logfile)
        case _:
            print('No such mode: ', args.mode)

    print('No errors: ', no_error_cnt)
    print('crc_only:  ', crc_only)
    print('corrected: ', corrected_cnt)
    print('uncorrect  ', uncorr_err_cnt)
    print('critcal:   ', bad_err_cnt)
