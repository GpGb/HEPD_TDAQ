import argparse
import configparser
import traceback
import struct
import math
from functools import partialmethod

import colorize
import daq_platform
from daq_platform import cmd_package


COLOR_OUT = True
RAW_TEST = False

def index_to_chipid(index):
    stave_id = index//10

    chip_id_lsb = index%10
    if (chip_id_lsb > 4):
        chip_id_lsb += 3

    chip_id = 0x70 | (stave_id<<8) | chip_id_lsb
    return chip_id



class daq_board():


    def  __init__(self, platform, host='192.168.1.10', port=10):

        if platform == 'FTDI':
            self.comm = daq_platform.daq_comm_ftdi(COLOR_OUT=COLOR_OUT)
        elif platform == 'TDAQ':
            self.comm = daq_platform.daq_comm(HOST=host, PORT=port,
                COLOR_OUT=COLOR_OUT, no_bread=False)
        elif platform == 'EGSE':
            # somewhat unnecessary since TDAQ tolerates any value as EOP
            cmd_package.__init__ = partialmethod(cmd_package.__init__, EOP=4)
            self.comm = daq_platform.daq_comm(HOST=host, PORT=port,
                COLOR_OUT=COLOR_OUT, no_bread=True)
        else:
            raise Exception(f'Unkown platform {platform}')

        self.pkt_count = 0


    def read_reg(self, addr):
        data = self.comm.read_reg(addr)
        addr = hex(addr)
        data = hex(data)
        print(f'reg: {addr}, data: {data}')


    def write_reg(self, addr, data):
        ret = self.comm.write_reg(addr, data)
        data = hex(data)
        print(f'wr reg: {addr}, data: {data}')


    def write_fpga_reg(self, addr, value):

        command = bytearray()
        command.append(0x6)
        command += addr.to_bytes(4, byteorder='little')
        command += value.to_bytes(4, byteorder='little')

        print(f'addr:{addr}, wr: {value}')
        ret = self.comm.write_command(command)
        return ret


    def write_alp_reg(self, chip_id, addr, value):

        command = bytearray()
        command.append(0x5)
        command.append(0x9c)
        command += chip_id.to_bytes(2, byteorder='little')
        command += addr.to_bytes(2, byteorder='little')
        command += value.to_bytes(2, byteorder='little')

        print(f'addr:{addr}, wr: {value}')
        ret = self.comm.write_command(command)
        return ret


    def read_alp_reg(self, chip_id, addr):

        command = bytearray()
        command.append(0x5)
        command.append(0x4e)
        command += chip_id.to_bytes(2, byteorder='little')
        command += addr.to_bytes(2, byteorder='little')
        command += bytes(2)

        self.comm.write_command(command)
        data = self.comm.read_cmd_response()

        data = hex(data[0]&0xffff)
        print(f'addr:{addr}, val: {data}')
        return 0


    def read_chipmask(self, stave_id):
       command = bytearray()
       command.append(0x0f)
       command.append(stave_id)

       self.comm.write_command(command)
       data = self.comm.read_cmd_response()

       return data[0]&0x3fff


    def print_chipmask(self):

        for i in range(15):
            data = self.read_chipmask(i)
            print(f'stave: {i:02}, mask: {data:010b}')


    def write_chipmask(self, stave_id, chipmask):

        command = bytearray()
        command.append(0x0e)
        command.append(stave_id)
        command += (chipmask).to_bytes(2, byteorder='little')

        self.comm.write_command(command)
        data = self.comm.read_cmd_response()[0]


    def ping(self):

        self.comm.write_reg(cmd_package.CMD_ADDR, 0x1)
        status = self.comm.wait_done()

        if status:
            return -1

        errors = 0;
        counter = 0xffffffff
        for i in range(0x2002, 0x2066):
            data = self.comm.read_reg(i)
            if data != counter:
                errors +=1
            counter -= 1

        if errors == 0:
            print('OK')
        else:
            print('Err: ', errors)

        return 0


    def run_stave_conf(self, stave_id, findex):
        print(f'Init stave {stave_id}, with file {findex}')
        command  = 0x03 | (stave_id<<8) | (findex<<16)
        self.comm.write_reg(cmd_package.CMD_ADDR, command)
        status = self.comm.wait_done()


    def alpide_init(self, stave_id, findex):

        if stave_id == 0xf:
            for stave in range(15):
                self.run_stave_conf(stave, findex)
        else:
            self.run_stave_conf(stave_id, findex)


    def chip_scan(self):
        command  = 0x02
        self.comm.write_reg(cmd_package.CMD_ADDR, command)

        data = self.comm.read_cmd_response(raw=True)

        fmt = str(len(data)//2) + 'H'
        data = struct.unpack(fmt, data)

        if COLOR_OUT:
            for turret in range(5):
                colorize.gen_turret_render(turret, data)
            return

        for stave in range(15):

            for index in range(10):
                chip_id = index_to_chipid(index+stave*10)

                if (data[stave] & (1<<index)) != 0:
                    print('  -----', end='')
                else:
                    print(f'  {chip_id:#05x}', end='')

            print()


    def fifo_test(self, chip_id):
        command = 0x13 | (chip_id<<8)
        self.comm.write_reg(cmd_package.CMD_ADDR, command)

        data = self.comm.read_cmd_response()
        print('errors:', data)


    def dump_alp_regs(self, chip_id):
        command = 0x0b | (chip_id<<8)
        self.comm.write_reg(cmd_package.CMD_ADDR, command)

        data = self.comm.read_cmd_response(raw=True)
        print(len(data))
        fmt = str(len(data)//2) + 'H'
        data = struct.unpack(fmt, data)

        index = 0
        for n in range(1, 0x1b+1):
            print(hex(chip_id), hex(n), hex(data[index]))
            index += 1
        for n in range(0x600, 0x610+1):
            print(hex(chip_id), hex(n), hex(data[index]))
            index += 1


    def run_thrscan(self, stave_id, mode):
        PULSES = 20
        CHARGE_STEPS = 30

        chipmask = self.read_chipmask(stave_id)
        chip_tot = 10 - bin(chipmask).count('1')
        if chip_tot == 0:
            return

        command = bytearray()
        command.append(0x04)
        if RAW_TEST:
            mode |= (1<<7)
        command.append(mode)
        command.append(stave_id)

        self.comm.write_command(command)

        if RAW_TEST:
            mode &= 0xf
            ftype = 4 | (CHARGE_STEPS<<8) | (PULSES<<16)

            if mode == 0:
                STEP_SIZE = 30
            else:
                STEP_SIZE = 512

            pkt_num = PULSES*CHARGE_STEPS*STEP_SIZE * chip_tot
            ftype = 4 | (CHARGE_STEPS<<8) | (PULSES<<16) | (mode<<24)
            self.read_event(pkt_num=pkt_num, filemeta=ftype)
            return

        status = self.comm.wait_done(color = 'thr')
        if status:
            return

        data = self.comm.read_cmd_response(raw=True)

        fmt = str(len(data)) + 'B'
        data = struct.unpack(fmt, data)

        thrdata = []
        for x in range(0, len(data), 2):
            avg = data[x]
            std = data[x+1]
            thrdata.append((avg, std))

        if COLOR_OUT:
            colorize.thr_result(thrdata)
        else:
            for x in range(len(thrdata)):
                print(index_to_chipid(i), x)


    def run_thresh_cal(self, stave_id, vcasn0, target):
        command = bytearray()
        command.append(0x14)
        command.append(stave_id)
        command += vcasn0.to_bytes(2, byteorder='little')
        command.append(target)

        self.comm.write_command(command)
        status = self.comm.wait_done()
        if status:
            return

    def save_DAC_val(self, stave_id, findex):
        command = bytearray()
        command.append(0xa4)
        command.append(stave_id)
        command.append(findex)

        self.comm.write_command(command)
        status = self.comm.wait_done()


    def run_digiscan(self, stave_id):

        chipmask = self.read_chipmask(stave_id)
        chip_tot = 10 - bin(chipmask).count('1')
        if chip_tot == 0:
            return

        command = bytearray()
        command.append(0x04)
        # set mode
        if RAW_TEST:
            command.append(0x03|(1<<7))
        else:
            command.append(0x03)
        command.append(stave_id)

        self.comm.write_command(command)

        if RAW_TEST:
            self.read_event(pkt_num=5120*chip_tot, filemeta=2)
            return

        status = self.comm.wait_done(color = 'digi')
        if status:
            return

        data = self.comm.read_cmd_response();

        if COLOR_OUT:
            colorize.digiscan_result(chipmask, data)
        else:
            print(data)


    def hotpix_scan(self, stave_id, thresh, n_trigger):
        command = bytearray()
        command.append(0x08)
        command.append(stave_id)
        command.append(thresh)
        command += n_trigger.to_bytes(4, byteorder='little')

        self.comm.write_command(command)
        status = self.comm.wait_done(timeout=300)

        if status:
            print("Failed")
        else:
            print("Complete")


    def read_temp(self):
        STAVES = 15

        temp_offset  = 0x23cc
        # Read temp area "Header"
        area_size = self.comm.read_reg(temp_offset)
        tstamp    = self.comm.read_reg(temp_offset+1)
        print(f'len: {area_size}, tstamp: {tstamp}', end='\n\n')

        if area_size == 0:
            print('Err: No data')
            return 0

        temp_offset += 2

        temp_data = bytearray()
        # read raw temp data
        for i in range(math.ceil(area_size/4)):
            temp_data += self.comm.read_reg(temp_offset+i, raw=True)

        index = 0;
        for stave_id in range(STAVES+1):

            print(f'------ Stave id: {stave_id} -------')
            if stave_id == 0xf:
                n_sensors = 2
            elif stave_id%3 == 1:
                n_sensors = 7
            else:
                n_sensors = 6

            for n in range(n_sensors):
                # there is probably a better way..
                data = temp_data[index:index+2]
                if data == b'\xff\xff':
                    print("NaN", end='\t\t')
                else:
                    data = b'\x00\x00' + data # exted to 4 bytes
                    temp = struct.unpack('>l', data)[0]/16
                    print(temp, end='\t\t')

                index += 2

            print()

        return 0


    def set_temp_mask(self, stave_id, mask):
        command = bytearray()
        command.append(0x0d)
        command.append(stave_id)
        command.append(mask)
        ret = self.comm.write_command(command)

        return self.comm.wait_done()



    def format_flash(self):

        self.comm.write_reg(cmd_package.CMD_ADDR, 0x11)
        status = self.comm.wait_done()

        print('done: ', hex(status))


    def write_file(self, findex, data, mode):

        print("file len: ", len(data))
        command = bytearray()
        command.append(0x09)
        command.append(findex)
        command += len(data).to_bytes(2, byteorder='little')
        command.append(mode)

        command += data

        ret = self.comm.write_command(command)

        return self.comm.wait_done()


    def read_file(self, findex):

        command = 0x10 | (findex<<8)
        self.comm.write_reg(cmd_package.CMD_ADDR, command)
        data = self.comm.read_cmd_response(raw=True)

        if len(data) <= 1:
            print('File is empty')
            return 0

        file_len =  int.from_bytes(data[0:4], byteorder='little')
        print(f'File lenght: {file_len}')
        # skip len
        data = data[4:]

        if findex < 5:

            n = 0
            for i in range(0, len(data), 2):
                val = int.from_bytes(data[i:i+2], byteorder='little')
                print(hex(val), end=' ')
                n += 1
                if (n%3==0): print('')

        elif findex == 5:

            for i in range(0, len(data), 4):
                word = int.from_bytes(data[i:i+4], byteorder='little')
                unmskble = word>>31
                staveid  = (word>>24)&0xf
                id_lsb   = (word>>20)&0xf
                pix_y    = (word>>10)&0x3ff
                pix_x    = word&0x3ff
                chip_id = hex(0x70 | (staveid<<8) | id_lsb)

                pline = f'chip_id: {chip_id}  col,row: {pix_x},{pix_y}'
                if unmskble == 1:
                    pline += " ** Unmaskable **"
                print(pline)

        else:
            print(data)


        return 0;


    def read_event(self, pkt_num = 0, filemeta = 1):

        rawfile = open('output.raw', 'wb')
        rawfile.write(int.to_bytes(filemeta, 4, byteorder='little'))

        self.comm.write_reg(0x9, 0xff)

        while (self.pkt_count < pkt_num or pkt_num == 0):

            try:
                data = self.comm.read_data_pkg()
                wr_len = rawfile.write(data)

                if wr_len > 0:
                    self.pkt_count += 1
                    if pkt_num == 0:
                        print(f'pktcnt: {self.pkt_count}', end='\r')
                    else:
                        progress = self.pkt_count/pkt_num*100
                        print(f'progress: {progress:0.2f}%', end='\r')

            except KeyboardInterrupt:
                print('\nInterrupted')
                break
            except:
                print('Unkown error')
                print(traceback.format_exc())
                break

        self.comm.write_reg(0x9, 0x00)
        rawfile.close()
        print()


    def soft_reset(self, reset_type):
        if reset_type == "global":
            self.comm.write_reg(0x3, 0x55555555);
        elif  reset_type == "daq":
            self.comm.write_reg(0x3, 0x0da00da0);
        else:
            print("Reset must be 'global' or 'daq'")




def parse_config(tdaq, inifile, write_flash=None):

    ALPIDE_reg_addr = {
        'mode_control'    : 0x1,
        'fromu_conf_1'    : 0x4,
        'fromu_conf_2'    : 0x5,
        'fromu_conf_3'    : 0x6,
        'fromu_pulsing_1' : 0x7,
        'fromu_pulsing_2' : 0x8,
        'cmu_dmu'         : 0x10,
        'pixcfg'    : 0x500,
        'vresetp'   : 0x601,
        'vresetd'   : 0x602,
        'vcasp'     : 0x603,
        'vcasn'     : 0x604,
        'vcasn2'    : 0x607,
        'vclip'     : 0x608,
        'vtemp'     : 0x609,
        'iaux2'     : 0x60a,
        'ireset'    : 0x60b,
        'idb'       : 0x60c,
        'ibias'     : 0x60d,
        'ithr'      : 0x60e
    }

    parser = configparser.ConfigParser(
            converters={'intfromstr': lambda x: int(x, 0)})

    parser.read(inifile)
    sections = parser.sections()

    bindata = bytearray()

    for section in sections:
        for key in parser[section]:
            chip_id = int(section, 0)
            address = ALPIDE_reg_addr[key]
            value   = parser.getintfromstr(section, key)

            if write_flash != None:
                bindata += (chip_id).to_bytes(2, byteorder='little')
                bindata += (address).to_bytes(2, byteorder='little')
                bindata += (value).to_bytes(2, byteorder='little')
            else:
                tdaq.write_reg(chip_id, address, value)

            print(f'cid {section}, {key}: {address} = {value}')

    if write_flash != None:
        tdaq.write_file(write_flash, bindata, 0)



if __name__ == '__main__':

    def str_to_int(x):
        return int(x, 0)

    parser = argparse.ArgumentParser()

    parser.add_argument('-P', '--platform', default="FTDI",
            help='DAQ platform (FTDI, TDAQ or EGSE)')
    parser.add_argument('--host', default='192.168.1.10',
            help='Host for spw server (TDAQ or EGSE mode)')

    parser.add_argument('-p', action='store_true', default=False,
            help='ping test')
    parser.add_argument('-i', type=str_to_int, nargs=2, metavar=('STAVEID', 'FINDEX'),
            help='run init routine')
    parser.add_argument('-a', action='store_true', default=False,
            help='chip scan')

    parser.add_argument('--read-reg', type=str_to_int,
            metavar='ADDR', help='read regfile reg')
    parser.add_argument('--write-reg', type=str_to_int, nargs=2,
            metavar = ('ADDR', 'VAL'), help='write regfile reg')

    parser.add_argument('--write-fpga-reg', type=str_to_int, nargs=2,
            metavar = ('ADDR', 'VAL'), help='write fpga reg')

    parser.add_argument('--read-alp-reg', type=str_to_int, nargs=2,
            metavar = ('CHIPID', 'ADDR'), help='read alpide reg')
    parser.add_argument('--write-alp-reg', type=str_to_int, nargs=3,
            metavar = ('CHIPID', 'ADDR', 'VAL'), help='write alpide reg')
    parser.add_argument('--dump-alp-regs', type=str_to_int,
            metavar = ('CHIPID'), help='dump selected chipID regs')

    parser.add_argument('--read-chipmask', action='store_true', default=False,
            help='read chipmask')
    parser.add_argument('--write-chipmask', type=str_to_int, nargs=2,
            metavar = ('STAVEID', 'MASK'), help='write staveid chipmask')

    parser.add_argument('--read-temp', action='store_true', default=False,
            help='read dallas temps')
    parser.add_argument('--set-temp-mask', type=str_to_int, nargs=2,
            metavar = ('STAVEID', 'MASK'), help='set ignore mask for dallas sensors')

    parser.add_argument('--conf-file',
            metavar = 'CONF.INI', help='read and apply config.ini file')
    parser.add_argument('--write-flash', type=str_to_int, metavar = ('FINDEX'),
            help='write config.ini file to flash instead of applying')
    parser.add_argument('--read-file', type=str_to_int,
            metavar = 'FINDEX', help='dump file')
    parser.add_argument('--trunc-file', type=str_to_int,
            metavar = 'FINDEX', help='delete file')
    parser.add_argument('--format-flash', action='store_true', default=False,
            help='format flash')

    parser.add_argument('--fifo-test', type=str_to_int,
            metavar = 'CHIPID', help='fifo test')

    parser.add_argument('-T', '--thrscan', type=str_to_int,
            metavar = 'STAVEID', help='run threshold scan')
    parser.add_argument('--fast-thrscan', type=str_to_int,
            metavar = 'STAVEID', help='run threshold scan')
    parser.add_argument('--thresh-cal', type=str_to_int, nargs=3,
            metavar = ('STAVEID', 'VCANS0', 'TARGET'),
            help='run threshold calibration, using VCANS0 as start value. TARGET is in charge steps.')
    parser.add_argument('--save-DAC-val', type=str_to_int, nargs=2,
            metavar = ('STAVEID', 'FINDEX'),
            help='Save the current values of the DAC regs in the selected file')

    parser.add_argument('-D', '--digiscan', type=str_to_int,
            metavar = 'STAVEID', help='run digital scan')
    parser.add_argument('-H', type=str_to_int, nargs=3,
            metavar = ('STAVEID', 'THRESH', 'NTRIGS'),
            help='hot pixels scan')
    parser.add_argument('--raw-test', action='store_true', default=False,
            help='download digi/thrscan raw data')

    parser.add_argument('-q', action='store_true', default=False,
            help='start daq mode')

    parser.add_argument('-0', '--reset', metavar = 'TYPE',
            help='fpga global reset, TYPE is \'global\' or \'daq\'')

    parser.add_argument('--nocolor', action='store_true', default=False,
            help='disable colorized output')

    args = parser.parse_args()


    COLOR_OUT = not args.nocolor
    RAW_TEST = args.raw_test

    daq = daq_board(args.platform, host=args.host)

    if args.p: daq.ping()
    if args.i != None: daq.alpide_init(*args.i)
    if args.a: daq.chip_scan()

    if args.read_temp: daq.read_temp()
    if args.set_temp_mask != None: daq.set_temp_mask(*args.set_temp_mask)

    if args.read_reg != None: daq.read_reg(args.read_reg)
    if args.write_reg: daq.write_reg(*args.write_reg)
    if args.write_fpga_reg: daq.write_fpga_reg(*args.write_fpga_reg)

    if args.read_alp_reg: daq.read_alp_reg(*args.read_alp_reg)
    if args.write_alp_reg: daq.write_alp_reg(*args.write_alp_reg)
    if args.dump_alp_regs: daq.dump_alp_regs(args.dump_alp_regs)

    if args.read_chipmask: daq.print_chipmask()
    if args.write_chipmask: daq.write_chipmask(*args.write_chipmask)

    if args.conf_file: parse_config(daq, args.conf_file,
            write_flash=args.write_flash)


    if args.read_file != None: daq.read_file(args.read_file)
    if args.trunc_file != None: daq.write_file(args.trunc_file, bytes(), 0)

    if args.format_flash: daq.format_flash()
    if args.fifo_test: daq.fifo_test(args.fifo_test)
    if args.thrscan != None: daq.run_thrscan(args.thrscan, 1)
    if args.fast_thrscan != None: daq.run_thrscan(args.fast_thrscan, 0)
    if args.thresh_cal != None: daq.run_thresh_cal(*args.thresh_cal)
    if args.save_DAC_val != None: daq.save_DAC_val(*args.save_DAC_val)

    if args.digiscan != None: daq.run_digiscan(args.digiscan)
    if args.H: daq.hotpix_scan(*args.H)

    if args.q: daq.read_event()
    if args.reset: daq.soft_reset(args.reset)
