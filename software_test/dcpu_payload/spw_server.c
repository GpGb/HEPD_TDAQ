#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/ioctl.h>

#include <signal.h>

#include <errno.h>
#include <string.h>


#define SPW_BASEADDR  0x40c40000
#define SPW_ADDR_OFF  1
#define SPW_DATA_OFF  2
#define SPW_RX_OFF    3
#define SPW_CMD_OFF   4
#define SPW_STAT_OFF  5
#define SPW_BURST_OFF 6
#define SPW_TAIL_OFF  13
#define SPW_DPRAM_OFF 28

#define SPW_FSM_IDLE  0x69646c65
#define SPW_FSM_RDOP  0x03
#define SPW_FSM_WROP  0x0c
#define SPW_FSM_BRDOP 0xee

#define MMAP_SIZE     sysconf(_SC_PAGESIZE)*2
#define RX_DATA_MAX   800


int mem_fd = 0;
volatile uint32_t *spw_mstr_ba = NULL;
int sock_fd = 0;

int log_level = 0;

// ******** signal handler *********
static bool terminate = false;
void sigint_handler(int sig) {
	terminate = 1;
}
// *********************************


int cbuffer_read(uint32_t* data, uint32_t wlen){

	const uint32_t buff_top = 0x400;
	uint32_t read_pointer = spw_mstr_ba[SPW_TAIL_OFF];

	if (wlen <= read_pointer)
		read_pointer -= wlen;
	else
		read_pointer = buff_top-(wlen-read_pointer);


	for(int n=0; n<wlen; n++){
		data[n] = spw_mstr_ba[SPW_DPRAM_OFF + read_pointer];
		read_pointer++;
		if (read_pointer >= buff_top) read_pointer = 0;
	}

	return wlen;
}


int spw_send_cmd(uint8_t opcode, uint32_t addr, uint32_t data, uint32_t *rcv_data){

	if (log_level > 1){
		printf("snd cmd: %x, addr: %x, dat: %x\n", opcode, addr, data);
	}

	// execute transmisison
	spw_mstr_ba[SPW_CMD_OFF] = 0;
	spw_mstr_ba[SPW_ADDR_OFF] = addr;
	spw_mstr_ba[SPW_DATA_OFF] = data;
	if (opcode == SPW_FSM_BRDOP){
		spw_mstr_ba[SPW_BURST_OFF] = data;
	}
	spw_mstr_ba[SPW_CMD_OFF] = opcode;

	// wait for execution
	while(spw_mstr_ba[SPW_STAT_OFF] != SPW_FSM_IDLE);

	if (rcv_data == NULL){
		return 0;
	}

	// read response if any
	uint32_t wlen = 0;

	if (opcode == SPW_FSM_RDOP){
		wlen = 1;
		*rcv_data = spw_mstr_ba[SPW_RX_OFF];
	}
	else if (opcode == SPW_FSM_BRDOP){
		wlen = data;
		cbuffer_read(rcv_data, wlen);
	}

	return wlen;
}


int daq_loop(int conn_fd){

	int run_daq = true;
	uint32_t rcv_data[RX_DATA_MAX];

	printf("Starting DAQ loop..\n");

	uint32_t pkt_size=0;

	while(run_daq){

		// any command from client interrupts the daq
		int count = 0;
		ioctl(conn_fd, FIONREAD, &count);
		if (count > 0){
			run_daq = false;
		}

		// poll for data
		int ret = spw_send_cmd(SPW_FSM_RDOP, 0x1000, 0, rcv_data);
		pkt_size = rcv_data[0];

		if (ret != 1 || pkt_size == 0){
			//usleep(1000)
			continue;
		}

		if (log_level > 1){
			printf("recv pkt len: %d\n", pkt_size);
		}

		// read data
		ret = spw_send_cmd(SPW_FSM_BRDOP, 0x1001, pkt_size, rcv_data);

		if (log_level == 1){

			if ( (rcv_data[0]>>16) != 0xffff ){
				printf("recv pkt len: %d\n", pkt_size);
				printf("BAD header: %#x\n", rcv_data[0]);
			}

			if (pkt_size != ret){
				printf("Read wrong len. pkt_size: %#x, read: %#x\n", pkt_size, ret);
			}

			if (pkt_size*sizeof(uint32_t) != (rcv_data[0]&0xffff) + 16){
				printf("BAD len: %#x, header: %#x\n", ret, rcv_data[0]);
			}

		}

		// send data to client
		if (ret > 0){
			send(conn_fd, &rcv_data, ret*sizeof(uint32_t), 0);
		}

		// clear data
		ret = spw_send_cmd(SPW_FSM_WROP, 0x1000, 0, NULL);
	}

	spw_send_cmd(SPW_FSM_WROP, 0x09, 0x0, rcv_data);
	printf("Stopped DAQ loop.\n");
	return 0;
}


int create_socket(int listen_port){

	sock_fd = socket(AF_INET, SOCK_STREAM, 0);
	if (sock_fd < 0) {
		printf("Could not create socket\n");
		return -1;
	}

	struct sockaddr_in server_addr;
	memset(&server_addr, 0, sizeof(server_addr));

	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.s_addr = INADDR_ANY;
	server_addr.sin_port = htons(listen_port);

	if (bind(sock_fd, (struct sockaddr*)&server_addr, sizeof(server_addr))) {
		printf("Failed to bind socket: %s\n", strerror(errno));
		return -1;
	}

	if (listen(sock_fd, 5)){
		printf("Failed to listen on socket: %s\n", strerror(errno));
		return -1;
	}

	return 0;

}


int serve_client_loop(int conn_fd){

	int ret = 0;
	uint8_t buffer[1000];
	uint32_t rx_data[RX_DATA_MAX];

	while(1){
		ret = recv(conn_fd, buffer, sizeof(buffer), 0);
		if (ret <= 0){
			break;
		}

		if (ret%10 != 0){
			printf("partial pkt received\n");
			continue;
		}

		for(int n=0; n<ret; n+=10){
			uint8_t opcode = 0;
			uint32_t addr = 0, data = 0;
			memcpy(&opcode, buffer+n, 1);
			memcpy(&addr, buffer+n+1, 4);
			memcpy(&data, buffer+n+5, 4);

			int ret = spw_send_cmd(opcode, addr, data, rx_data);

			if (opcode == SPW_FSM_WROP && addr == 0x9 && data != 0){
				daq_loop(conn_fd);
			}

			if (ret > 0){
				send(conn_fd, &rx_data, ret*sizeof(uint32_t), 0);
			}
		}

	}

	return ret;
}


void cleanup(){
	close(sock_fd);

	if (spw_mstr_ba != NULL || spw_mstr_ba != MAP_FAILED)
		munmap((void*)spw_mstr_ba, MMAP_SIZE);

	close(mem_fd);
}


int main(int argc, char *argv[]) {

	signal(SIGINT, sigint_handler);

	int opt;
	int port = 10;

	while ((opt = getopt(argc, argv, "vp:")) != -1) {
		switch (opt) {
			case 'v':
				log_level += 1;
				break;
			case 'p':
				port = atoi(optarg);
				break;
			default:
				printf("Usage: %s [-vv]\n", argv[0]);
				return -1;
		}
	}


	if (create_socket(port)){
		return -1;
	}

	mem_fd = open("/dev/mem", O_RDWR);
	if (mem_fd < 0){
		printf("Can't open /dev/mem: %s\n", strerror(errno));
		cleanup();
		return -1;
	}

	spw_mstr_ba = (volatile uint32_t*) mmap(NULL, MMAP_SIZE, PROT_READ | PROT_WRITE,
			MAP_SHARED,	mem_fd, SPW_BASEADDR);

	if (spw_mstr_ba == MAP_FAILED){
		printf("mmap failed: %s\n", strerror(errno));
		cleanup();
		return -1;
	}


	while(!terminate){
		printf("Waiting for connections..\n");
		int conn_fd = accept(sock_fd, NULL, NULL);
		printf("Conn accepted\n");

		serve_client_loop(conn_fd);
		close(conn_fd);
		printf("Closed connection\n\n");
	}

	cleanup();
	return 0;
}
